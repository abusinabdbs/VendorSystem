﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Bs.VendorSystem.Models;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Bs.VendorSystem.Validators
{
    public class VendorRegisterValidator : BaseNopValidator<VendorRegisterModel>
    {
        public VendorRegisterValidator(ILocalizationService localizationService, CustomerSettings customerSettings,
            IStateProvinceService stateProvinceService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            RuleFor(x => x.FirstName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.FirstName.Required"));
            RuleFor(x => x.LastName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.LastName.Required"));
            RuleFor(x => x.Password).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Password.Required"));
            RuleFor(x => x.Password).Length(customerSettings.PasswordMinLength, 999).WithMessage(string.Format(localizationService.GetResource("Account.Fields.Password.LengthValidation"), customerSettings.PasswordMinLength));
            RuleFor(x => x.ConfirmPassword).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.ConfirmPassword.Required"));
            RuleFor(x => x.ConfirmPassword).Equal(x => x.Password).WithMessage(localizationService.GetResource("Account.Fields.Password.EnteredPasswordsDoNotMatch"));

            //form fields

            RuleFor(x => x.CountryId)
                .NotEqual(0)
                .WithMessage(localizationService.GetResource("Account.Fields.Country.Required"));


            Custom(x =>
            {
                //does selected country have states?
                var hasStates = stateProvinceService.GetStateProvincesByCountryId(x.CountryId).Count > 0;
                if (hasStates)
                {
                    //if yes, then ensure that a state is selected
                    if (x.StateProvinceId == 0)
                    {
                        return new ValidationFailure("StateProvinceId", localizationService.GetResource("Account.Fields.StateProvince.Required"));
                    }
                }
                return null;
            });



            RuleFor(x => x.StreetAddress).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress.Required"));
            RuleFor(x => x.Phone).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Phone.Required"));

            RuleFor(x => x.BankName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.BankName.Required"));
            RuleFor(x => x.BankAccountName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.BankAccountName.Required"));

            RuleFor(x => x.BankAccountNumber).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.BankAccountNumber.Required"));
            RuleFor(x => x.AlternatePhoneNumber).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.AlternatePhoneNumber.Required"));

            RuleFor(x => x.StoreName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StoreName.Required"));
            RuleFor(x => x.Url).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Url.Required"));

            RuleFor(x => x.Describe).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Describe.Required"));
        }
    }
}
