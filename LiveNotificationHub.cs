﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem
{
    [HubName("LiveNotificationHub")]
    class LiveNotificationHub : Hub
    {
        [HubMethodName("LiveNotificationHub")]
        public static void LiveChat(string myMsg)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<LiveNotificationHub>();
            context.Clients.All.LiveChat(myMsg);
        }
    }
}
