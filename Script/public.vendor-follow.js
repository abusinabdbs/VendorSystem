﻿
var VendorFollow= {
    loadWaiting: false,
    usepopupnotifications: false,
    follow_unfollow_selector: '',

    init: function (usepopupnotifications, follow_unfollow_selector) {
        this.loadWaiting = false;
        this.usepopupnotifications = usepopupnotifications;
        this.follow_unfollow_selector = follow_unfollow_selector;

    },
    displayFollow: function () {
        $('.ajax-loading-block-window').hide('fast');
        setTimeout(function () { $('.follow-block-window').show(); }, 150);
        setTimeout(function () { $('.follow-block-window').hide('fast'); }, 650);
    },
    displayUnFollow: function () {
        $('.ajax-loading-block-window').hide('fast');
        setTimeout(function () { $('.un-follow-block-window').show(); }, 150);
        setTimeout(function () { $('.un-follow-block-window').hide('fast'); }, 650);
       
    },
    setLoadWaiting: function (display) {
        displayAjaxLoading(display);
        this.loadWaiting = display;
    },

    //add a product to the cart/wishlist from the catalog pages
    follow: function (urlfollow, follow_unfollow_selector) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);
        this.init(false, follow_unfollow_selector);
        $.ajax({
            cache: false,
            url: urlfollow,
            type: 'post',
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },


    unfollow: function (urlunfollow, follow_unfollow_selector) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);
        this.init(false, follow_unfollow_selector);
        $.ajax({
            cache: false,
            url: urlunfollow,
            type: 'post',
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },
    guestfollow: function () {
        displayPopupNotification("Please <a href=\"/login\" class=\"ico-login\" style=\"color:red;text-decoration: underline;\"> Log in</a> to follow the Vendor", 'Alert', true);

    },


    success_process: function (response) {
        if (response.message) {
            //display notification
            if (response.success == true) {
                //success
                if (response.vendorFollowedCountHtml) {
                    $(VendorFollow.follow_unfollow_selector).html(response.vendorFollowedCountHtml);
                    $('.follow-qty').html(response.followcounthtml);
                    $(VendorFollow.follow_unfollow_selector).toggle();
                }

                if (response.followproperty == true) {
                    VendorFollow.displayFollow();
                }
                else
                    VendorFollow.displayUnFollow();
            }
            else {
                //error
                if (VendorFollow.usepopupnotifications == true) {
                    displayPopupNotification(response.message, 'error', true);
                }
                else {
                    //no timeout for errors
                    displayBarNotification(response.message, 'error', 0);
                }
            }
            return false;
        }
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
        return false;
    },

    resetLoadWaiting: function () {
        VendorFollow.setLoadWaiting(false);
    },

    ajaxFailure: function () {
        alert('Failed to follow or unfollow the Vendor. Please refresh the page and try one more time.');
    }
};