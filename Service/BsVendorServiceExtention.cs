﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Service
{
    public static class BsVendorServiceExtention
    {
        public static string ToUrlString(this string content, bool changeToUtf)
        {
            if (!string.IsNullOrEmpty(content))
            {
                content = content.Replace(" ", "-").ToLower();
                if (changeToUtf)
                {
                    content = content.Replace("İ", "i");
                    content = content.Replace("ı", "i");
                    content = content.Replace("I", "i");
                    content = content.Replace("ö", "o");
                    content = content.Replace("ü", "u");
                    content = content.Replace("ğ", "g");
                    content = content.Replace("ş", "s");
                    content = content.Replace("ç", "c");
                    content = content.Replace("+", "ve");
                    content = content.Replace("'", "-");
                    content = content.Replace("\"", "-");
                    content = content.Replace(",", "-");
                    content = content.Replace("!", "-");
                    content = content.Replace("?", "-");
                    content = content.Replace("’", "-");
                    content = content.Replace("%", "yuzde");
                    content = content.Replace("=", "esittir");
                    content = content.Replace("(", "-");
                    content = content.Replace(")", "-");
                    content = content.Replace("*", "-");
                    content = content.Replace("&", "ve");
                }
            }
            return content;
        }

        public static ProductPicture FindProductPicture(this IList<ProductPicture> source,
int productId, int pictureId)
        {
            foreach (var productPicture in source)
                if (productPicture.ProductId == productId && productPicture.PictureId == pictureId)
                    return productPicture;

            return null;
        }
    }
}
