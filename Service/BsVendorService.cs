﻿using Nop.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Data.Mapping.Customers;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Bs.VendorSystem.Models;
using Nop.Services.Events;
using Nop.Services.Orders;


namespace Nop.Plugin.Bs.VendorSystem.Service
{
    public class BsVendorService : IBsVendorService
    {
        private readonly IRepository<BsVendorInfoTable> _bsVendorInfoRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IWorkContext _workContext;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IStoreContext _storeContext;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private const string CATEGORIES_BY_PARENT_CATEGORY_ID_KEY = "Nop.category.byparent-{0}-{1}-{2}-{3}-{4}";


        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Order> _orderRepository;

        private readonly IRepository<ProductPicture> _productPictureRepository;

        private readonly IRepository<LikeInfoTable> _likeInfoRepository;
        private readonly IRepository<BsVendorFollowInfoTable> _bsVendorFollowInfoRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<BsVendorNotificationInfoTable> _bsVendorNotificationInfoRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<CustomerRole> _customerRoleRepository;
        private readonly IRepository<Product> _productRepository;

        public BsVendorService(IRepository<BsVendorInfoTable> bsVendorInfoRepository,
            IRepository<Customer> customerRepository,
            IRepository<Category> categoryRepository,
            IWorkContext workContext,
            IRepository<AclRecord> aclRepository,
            IStoreContext storeContext,
            IRepository<StoreMapping> storeMappingRepository,
            IRepository<Order> orderRepository,
            IRepository<ProductPicture> productPictureRepository,
            IRepository<LikeInfoTable> likeInfoRepository,
            IRepository<BsVendorFollowInfoTable> bsVendorFollowInfoRepository,
            IEventPublisher eventPublisher,
            IRepository<BsVendorNotificationInfoTable> bsVendorNotificationInfoRepository,
            IRepository<OrderNote> orderNoteRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<CustomerRole> customerRoleRepository,
            IRepository<Product> productRepository)
        {
            this._bsVendorInfoRepository = bsVendorInfoRepository;
            this._customerRepository = customerRepository;
            this._categoryRepository = categoryRepository;
            this._workContext = workContext;
            this._aclRepository = aclRepository;
            this._storeContext = storeContext;
            this._storeMappingRepository = storeMappingRepository;
            this._orderRepository = orderRepository;
            this._productPictureRepository = productPictureRepository;
            this._likeInfoRepository = likeInfoRepository;
            this._bsVendorFollowInfoRepository = bsVendorFollowInfoRepository;
            this._eventPublisher = eventPublisher;
            this._bsVendorNotificationInfoRepository = bsVendorNotificationInfoRepository;
            this._orderNoteRepository = orderNoteRepository;
            this._orderItemRepository = orderItemRepository;
            this._customerRoleRepository = customerRoleRepository;
            this._productRepository = productRepository;
        }

        #region VendorInfo
        public void Insert(BsVendorInfoTable data)
        {
            _bsVendorInfoRepository.Insert(data);
        }
        public BsVendorInfoTable GetById(int id)
        {
            return _bsVendorInfoRepository.GetById(id);
        }
        public virtual BsVendorInfoTable GetVendorByVendorId(int vendorId)
        {

            if (vendorId == 0)
                return null;

            var query = from v in _bsVendorInfoRepository.Table
                        where v.VendorId == vendorId
                        select v;
            var vendor = query.FirstOrDefault();
            return vendor;


        }
        public void Update(BsVendorInfoTable data)
        {
            _bsVendorInfoRepository.Update(data);
        }
        public IPagedList<BsVendorInfoTable> GetAllNotApprovedVendors(int pageIndex = 0, int pageSize = Int32.MaxValue)
        {
            var query = _bsVendorInfoRepository.Table.Where(x => x.Approved == false);
            query = query.OrderBy(x => x.Id);
            var vendors = new PagedList<BsVendorInfoTable>(query, pageIndex, pageSize);
            return vendors;
        }
        public void Delete(int id)
        {
            BsVendorInfoTable data = _bsVendorInfoRepository.Table.FirstOrDefault(x => x.Id == id);
            _bsVendorInfoRepository.Delete(data);
        }
        public void Update(int productId, int customerId, bool delete = false)
        {


        }
        public virtual IList<Customer> GetCustomersByRoleId(int roleId)
        {
            if (roleId == 0)
                return null;

            var query = _customerRepository.Table;
            query = query.Where(c => !c.Deleted);
            //query = query.Where(c => c.CustomerRoles.Select(cr => cr.Id).Equals(roleId).Any());

            query = query.Where(c => c.CustomerRoles.Any(cr => cr.Id == roleId));

            query = query.OrderByDescending(c => c.LastActivityDateUtc);
            var customers = query.ToList();
            return customers;

        }
        public virtual IList<ProductPicture> GetProductPicturesByProductId(int productId)
        {
            var query = from pp in _productPictureRepository.Table
                        where pp.ProductId == productId
                        orderby pp.DisplayOrder
                        select pp;
            var productPictures = query.ToList();
            return productPictures;
        }
        public virtual OrderAverageReportLine GetOrderAverageReportLine(int storeId = 0,
     int vendorId = 0, int billingCountryId = 0,
     int orderId = 0, string paymentMethodSystemName = null,
     OrderStatus? os = null,
     DateTime? orderStatusChangeFromUtc = null, DateTime? orderStatusChangeToUtc = null,
     PaymentStatus? ps = null, ShippingStatus? ss = null,
     DateTime? startTimeUtc = null, DateTime? endTimeUtc = null,
     string billingEmail = null, string billingLastName = "",
     bool ignoreCancelledOrders = false, string orderNotes = null)
        {
            int? orderStatusId = null;
            if (os.HasValue)
                orderStatusId = (int)os.Value;

            int? paymentStatusId = null;
            if (ps.HasValue)
                paymentStatusId = (int)ps.Value;

            int? shippingStatusId = null;
            if (ss.HasValue)
                shippingStatusId = (int)ss.Value;


            var query = _orderRepository.Table;
            query = query.Where(o => !o.Deleted);
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (orderId > 0)
                query = query.Where(o => o.Id == orderId);
            if (vendorId > 0)
            {
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem => orderItem.Product.VendorId == vendorId));
            }
            if (billingCountryId > 0)
                query = query.Where(o => o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId);
            if (ignoreCancelledOrders)
            {
                var cancelledOrderStatusId = (int)OrderStatus.Cancelled;
                query = query.Where(o => o.OrderStatusId != cancelledOrderStatusId);
            }
            if (!String.IsNullOrEmpty(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);
            if (orderStatusId.HasValue)
                query = query.Where(o => o.OrderStatusId == orderStatusId.Value);
            string orderStatusNote = "";
            if (orderStatusId.HasValue)
            {
                query = query.Where(o => orderStatusId.Value == o.OrderStatusId);

                if (orderStatusId.Value == 10)
                    orderStatusNote = "Order placed";
                else if (orderStatusId.Value == 20)
                    orderStatusNote = "Order status has been edited. New status: Processing";
                else if (orderStatusId.Value == 30)
                    orderStatusNote = "Order status has been edited. New status: Complete";
                else
                    orderStatusNote = "Order status has been changed to Cancelled";
            }
            if (paymentStatusId.HasValue)
                query = query.Where(o => o.PaymentStatusId == paymentStatusId.Value);
            if (shippingStatusId.HasValue)
                query = query.Where(o => o.ShippingStatusId == shippingStatusId.Value);
            if (startTimeUtc.HasValue)
                query = query.Where(o => startTimeUtc.Value <= o.CreatedOnUtc);
            if (endTimeUtc.HasValue)
                query = query.Where(o => endTimeUtc.Value >= o.CreatedOnUtc);
            if (!String.IsNullOrEmpty(billingEmail))
                query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
            if (!String.IsNullOrEmpty(billingLastName))
                query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.LastName) && o.BillingAddress.LastName.Contains(billingLastName));
            if (!String.IsNullOrEmpty(orderNotes))
                query = query.Where(o => o.OrderNotes.Any(on => on.Note.Contains(orderNotes)));

            if (orderStatusId.HasValue)
            {
                if (orderStatusChangeFromUtc.HasValue || orderStatusChangeToUtc.HasValue)
                {
                    if (orderStatusChangeFromUtc.HasValue && orderStatusChangeToUtc.HasValue)
                    {
                        query = (from x in query
                                 join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                                 where y.Note.Equals(orderStatusNote) && (y.CreatedOnUtc >= orderStatusChangeFromUtc && y.CreatedOnUtc <= orderStatusChangeToUtc)
                                 orderby y.CreatedOnUtc descending
                                 select x);
                    }
                    else if (orderStatusChangeFromUtc.HasValue)
                    {
                        query = (from x in query
                                 join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                                 where y.Note.Equals(orderStatusNote) && (y.CreatedOnUtc >= orderStatusChangeFromUtc)
                                 orderby y.CreatedOnUtc descending
                                 select x);
                    }
                    else if (orderStatusChangeToUtc.HasValue)
                    {
                        query = (from x in query
                                 join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                                 where y.Note.Equals(orderStatusNote) && (y.CreatedOnUtc <= orderStatusChangeToUtc)
                                 orderby y.CreatedOnUtc descending
                                 select x);
                    }

                }

                else
                {
                    query = (from x in query
                             join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                             where y.Note.Equals(orderStatusNote)
                             orderby y.CreatedOnUtc descending
                             select x);
                }
            }
            else
            {
                if (orderStatusChangeFromUtc.HasValue || orderStatusChangeToUtc.HasValue)
                {
                    if (orderStatusChangeFromUtc.HasValue && orderStatusChangeToUtc.HasValue)
                    {
                        query = (from x in query
                                 //join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                                 where ((x.CreatedOnUtc >= orderStatusChangeFromUtc) && (x.CreatedOnUtc <= orderStatusChangeToUtc))
                                 //  orderby y.CreatedOnUtc descending
                                 select x).Distinct().OrderByDescending(x => x.CreatedOnUtc);
                    }
                    else if (orderStatusChangeFromUtc.HasValue)
                    {
                        query = (from x in query
                                 // join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                                 where x.CreatedOnUtc >= orderStatusChangeFromUtc
                                 // orderby y.CreatedOnUtc descending
                                 select x).Distinct().OrderByDescending(x => x.CreatedOnUtc);
                    }
                    else if (orderStatusChangeToUtc.HasValue)
                    {
                        query = (from x in query
                                 // join y in _orderNoteRepository.Table on x.Id equals y.OrderId
                                 where x.CreatedOnUtc <= orderStatusChangeToUtc
                                 // orderby y.CreatedOnUtc descending
                                 select x).Distinct().OrderByDescending(x => x.CreatedOnUtc);
                    }

                }

                else
                {
                    query = query.OrderByDescending(o => o.CreatedOnUtc);
                }
            }

            var item = (from oq in query
                        group oq by 1 into result
                        select new
                        {
                            OrderCount = result.Count(),
                            OrderShippingExclTaxSum = result.Sum(o => o.OrderShippingExclTax),
                            OrderTaxSum = result.Sum(o => o.OrderTax),
                            OrderTotalSum = result.Sum(o => o.OrderTotal)
                        }
                       ).Select(r => new OrderAverageReportLine
                       {
                           CountOrders = r.OrderCount,
                           SumShippingExclTax = r.OrderShippingExclTaxSum,
                           SumTax = r.OrderTaxSum,
                           SumOrders = r.OrderTotalSum
                       })
                       .FirstOrDefault();

            item = item ?? new OrderAverageReportLine
            {
                CountOrders = 0,
                SumShippingExclTax = decimal.Zero,
                SumTax = decimal.Zero,
                SumOrders = decimal.Zero,
            };
            return item;
        }

        public virtual Customer GetCustomerByVendorEmail(string vendorEmail)
        {
            if (string.IsNullOrEmpty(vendorEmail))
                return null;

            var query = from customer in _customerRepository.Table
                        orderby customer.Id
                        where customer.Email == vendorEmail
                        select customer;
            return query.FirstOrDefault();
        }

        #endregion

        #region BsLike
        public void InsertLike(LikeInfoTable data)
        {
            _likeInfoRepository.Insert(data);
            // _eventPublisher.Publish(new BsVendorLikeEvent(liked));
        }
        public void DeleteLike(int id)
        {
            LikeInfoTable data = _likeInfoRepository.Table.Where(x => x.Id == id).FirstOrDefault();
            _likeInfoRepository.Delete(data);
        }
        public void UpdateLike(int productId, int customerId, bool delete = false)
        {
            //LikeInfoTable liked = null;
            if (delete == true)
            {
                var data = _likeInfoRepository.Table.FirstOrDefault(x => x.ProductId == productId && x.CustomerId == customerId);
                //liked = data;
                _likeInfoRepository.Delete(data);
                _eventPublisher.Publish(new BsVendorDisLikeEvent(data));
            }
            else
            {
                var data = new LikeInfoTable()
                {
                    CustomerId = customerId,
                    ProductId = productId,
                    Islike = true
                };

                InsertLike(data);
                _eventPublisher.Publish(new BsVendorLikeEvent(data));
            }


        }
        public void DeleteOldData(DateTime time)
        {
            //DateTime Time = time.AddSeconds(-70);
            //var ListData=_currenViewInfoRepository.Table.Where(x => x.LastView<Time).ToList();
            //foreach (var Data in ListData)
            //{
            //    Delete(Data);
            //}
        }
        public int GetCount(int productId)
        {
            return _likeInfoRepository.Table.Count(x => x.ProductId == productId && x.Islike);
        }
        public bool IsAvailable(int productId, int customerId)
        {
            return _likeInfoRepository.Table.Any(x => x.ProductId == productId && x.CustomerId == customerId);
        }
        public LikeInfoTable AvailbilityCheckWithData(int productId, int customerId)
        {
            return _likeInfoRepository.Table.FirstOrDefault(x => x.ProductId == productId && x.CustomerId == customerId);
        }
        public bool IsLikedByCustomer(int productId, int customerId)
        {
            LikeInfoTable singleData = new LikeInfoTable();
            var data = _likeInfoRepository.Table.Where(x => x.ProductId == productId && x.CustomerId == customerId);
            if (data.Count() > 1)
            {
                data = data.OrderBy(x => x.Id);
                var dataList = data.ToList();
                for (int i = 0; i < data.Count() - 1; i++)
                {
                    _likeInfoRepository.Delete(dataList[i]);
                }
                singleData = dataList.Last();
            }
            else
            {
                singleData = data.FirstOrDefault();
            }
            return singleData != null && singleData.Islike;
        }
        public int GetLikeCountOfCustomer(int customerId)
        {
            return _likeInfoRepository.Table.Count(x => x.CustomerId == customerId && x.Islike == true);
        }
        public IPagedList<LikeInfoTable> GetLikedProductByCustomer(int customerId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _likeInfoRepository.Table;
            query = query.Where(x => x.CustomerId == customerId && x.Islike == true);
            query = query.OrderByDescending(x => x.Id);
            var products = new PagedList<LikeInfoTable>(query, pageIndex, pageSize);
            return products;

        }
        public IPagedList<BsLikeProductCount> MostLikedProduct(int pageIndex = 0, int pageSize = int.MaxValue)
        {

            var query = _likeInfoRepository.Table.GroupBy(x => x.ProductId).OrderByDescending(x => x.Count()).
                      Select(group =>
                          new BsLikeProductCount
                          {
                              ProductId = group.Key,
                              Count = group.Count()
                          });
            var products = new PagedList<BsLikeProductCount>(query, pageIndex, pageSize);
            return products;
        }
        #endregion

        #region Bs-follow
        public void InsertFollow(BsVendorFollowInfoTable data)
        {
            _bsVendorFollowInfoRepository.Insert(data);
        }
        public void DeleteFollow(int id)
        {
            BsVendorFollowInfoTable data = _bsVendorFollowInfoRepository.Table.Where(v => v.Id == id).FirstOrDefault();
            _bsVendorFollowInfoRepository.Delete(data);
        }
        public void UpdateFollow(int vendoeId, int customerId, bool delete = false)
        {

            if (delete == true)
            {
                // var data = _bsVendorFollowInfoRepository.Table.FirstOrDefault(x => x.ProductId == productId && x.CustomerId == customerId);
                var data =
                    _bsVendorFollowInfoRepository.Table.FirstOrDefault(
                        c => c.VendorId == vendoeId && c.CustomerId == customerId);
                _bsVendorFollowInfoRepository.Delete(data);
                _eventPublisher.Publish(new BsVendorUnFollowEvent(data));

            }
            else
            {
                var data = new BsVendorFollowInfoTable()
                {
                    CustomerId = customerId,
                    VendorId = vendoeId,
                    IsFollow = true
                };
                InsertFollow(data);
                _eventPublisher.Publish(new BsVendorFollowEvent(data));
            }
        }
        public int GetFollowCount(int vendorId)
        {
            //return _likeInfoRepository.Table.Count(x => x.ProductId == productId && x.Islike);
            return _bsVendorFollowInfoRepository.Table.Count(c => c.VendorId == vendorId && c.IsFollow);
        }
        public bool IsAvailableVendor(int vendorId, int customerId)
        {
            return _bsVendorFollowInfoRepository.Table.Any(x => x.VendorId == vendorId && x.CustomerId == customerId);
        }
        public BsVendorFollowInfoTable AvailbleVendorCheckWithData(int vendorId, int customerId)
        {
            return _bsVendorFollowInfoRepository.Table.FirstOrDefault(x => x.VendorId == vendorId && x.CustomerId == customerId);
        }
        public bool IsFollowByCustomer(int vendorId, int customerId)
        {
            BsVendorFollowInfoTable singleData = new BsVendorFollowInfoTable();
            var data = _bsVendorFollowInfoRepository.Table.Where(x => x.VendorId == vendorId && x.CustomerId == customerId);
            if (data.Count() > 1)
            {
                data = data.OrderBy(x => x.Id);
                var dataList = data.ToList();
                for (int i = 0; i < data.Count() - 1; i++)
                {
                    _bsVendorFollowInfoRepository.Delete(dataList[i]);
                }
                singleData = dataList.Last();
            }
            else
            {
                singleData = data.FirstOrDefault();
            }
            return singleData != null && singleData.IsFollow;
        }
        public int GetFollowCountOfCustomer(int customerId)
        {
            return _bsVendorFollowInfoRepository.Table.Count(x => x.CustomerId == customerId && x.IsFollow == true);
        }
        public IPagedList<BsVendorFollowInfoTable> GetFollowVendorByCustomer(int customerId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _bsVendorFollowInfoRepository.Table;
            query = query.Where(x => x.IsFollow == true).Distinct();
            //query = query.Where(x => x.CustomerId == customerId && x.IsFollow == true);
            query = query.OrderByDescending(x => x.Id);
            var follows = new PagedList<BsVendorFollowInfoTable>(query, pageIndex, pageSize);
            return follows;

        }

        public IPagedList<BsVendorFollowInfoTable> GetFollowCustomerByVendorId(int vendorId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _bsVendorFollowInfoRepository.Table;
            var customerId = _workContext.CurrentCustomer.Id;
            query = query.Where(x => x.VendorId == vendorId && x.IsFollow == true).Distinct();
            query = query.OrderByDescending(x => x.Id);
            var follows = new PagedList<BsVendorFollowInfoTable>(query, pageIndex, pageSize);
            return follows;

        }
        public IPagedList<BsVendorFollowCount> MostFollowsVendor(int pageIndex = 0, int pageSize = int.MaxValue)
        {

            var query = _bsVendorFollowInfoRepository.Table.GroupBy(x => x.VendorId).OrderByDescending(x => x.Count()).
                      Select(group =>
                          new BsVendorFollowCount
                          {
                              VendorId = group.Key,
                              Count = group.Count()
                          });
            var follows = new PagedList<BsVendorFollowCount>(query, pageIndex, pageSize);
            return follows;
        }
        public int GetFollowCountOfVendor(int vendorId)
        {
            return _bsVendorFollowInfoRepository.Table.Count(x => x.VendorId == vendorId && x.IsFollow == true);
        }
        #endregion

        #region Bs-Vendor-Notificatio
        public void InsertVendorNotification(BsVendorNotificationInfoTable data)
        {
            _bsVendorNotificationInfoRepository.Insert(data);
        }
        public void InsertVendorNotification(IEnumerable<BsVendorNotificationInfoTable> data)
        {
            _bsVendorNotificationInfoRepository.Insert(data);
        }
        public void UpdateVendorNotification(BsVendorNotificationInfoTable data)
        {
            _bsVendorNotificationInfoRepository.Update(data);
        }
        public IPagedList<BsVendorNotificationInfoTable> GetAllNotificationByCustomerId(int customerId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _bsVendorNotificationInfoRepository.Table;
            query = query.Where(x => x.NotifyingCustomerId == customerId);
            query = query.OrderByDescending(x => x.NotificationCreateDateTimeUtc);
            var notification = new PagedList<BsVendorNotificationInfoTable>(query, pageIndex, pageSize);
            return notification;
        }
        public IPagedList<BsVendorNotificationInfoTable> GetNotificationByCustomerId(int customerId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _bsVendorNotificationInfoRepository.Table;
            query = query.Where(x => x.NotifyingCustomerId == customerId);
            query = query.OrderByDescending(x => x.NotificationCreateDateTimeUtc);
            query = query.OrderByDescending(x => x.IsViewed);
            var notification = new PagedList<BsVendorNotificationInfoTable>(query, pageIndex, pageSize);
            return notification;
        }
        public IPagedList<BsVendorNotificationInfoTable> GetUnViewNotificationByCustomerId(int customerId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _bsVendorNotificationInfoRepository.Table;
            query = query.Where(x => x.NotifyingCustomerId == customerId && x.IsViewed == false);
            query = query.OrderByDescending(x => x.NotificationCreateDateTimeUtc);
            var notification = new PagedList<BsVendorNotificationInfoTable>(query, pageIndex, pageSize);
            return notification;
        }
        public int CountUnViewingNotificationByCustomerId(int customerId)
        {
            var numberOfUnViewNotification =
                _bsVendorNotificationInfoRepository.Table.Count(
                    x => x.IsViewed == false && x.NotifyingCustomerId == customerId);

            return numberOfUnViewNotification;
        }

        public BsVendorNotificationInfoTable GetNotificationById(int id)
        {
            return _bsVendorNotificationInfoRepository.GetById(id);
        }


        public IQueryable<BsVendorNotificationInfoTable> GetNotifications()
        {
            return _bsVendorNotificationInfoRepository.Table;
        }

        public IQueryable<OrderItem> GetOrderItemByOrderId(int orderId)
        {
            return _orderItemRepository.Table.Where(x => x.OrderId == orderId);
        }

        public virtual IList<Customer> GetAllAdminCustomerByRollId(int[] customerRoleIds)
        {
            var query = _customerRepository.Table;
            if (customerRoleIds != null && customerRoleIds.Length > 0)
                query = query.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(customerRoleIds).Any());
            var adminCustomer = query.ToList();
            return adminCustomer;
        }

        public virtual CustomerRole GetAllAdminCustomerRole()
        {

            var query = _customerRoleRepository.Table;
            var customerRole = query.FirstOrDefault(c => c.Name == "Administrators");
                return customerRole;
            
        }
        #endregion

        #region VendorPage

        public int GetVendorFollowerCount(int vendorId)
        {
            return _productRepository.Table.Count(v => v.VendorId == vendorId);
        }

        public int GetVendorProductCount(int vendorId)
        {
            return _productRepository.Table.Count(v => v.VendorId == vendorId);
        }
        #endregion


    }
}
