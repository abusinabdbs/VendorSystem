﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Bs.VendorSystem.Models;


namespace Nop.Plugin.Bs.VendorSystem.Service
{
   public interface IBsVendorService
    {
      
       #region VendorInfo
        void Insert(BsVendorInfoTable data);
        BsVendorInfoTable GetById(int id);
        BsVendorInfoTable GetVendorByVendorId(int vendorId);
        void Update(BsVendorInfoTable data);
        IPagedList<BsVendorInfoTable> GetAllNotApprovedVendors(int pageIndex = 0, int pageSize = Int32.MaxValue);
        void Delete(int id);
        void Update(int productId, int customerId, bool delete = false);


        IList<Customer> GetCustomersByRoleId(int roleId);
        IList<ProductPicture> GetProductPicturesByProductId(int productId);
        OrderAverageReportLine GetOrderAverageReportLine(int storeId = 0, int vendorId = 0,
            int billingCountryId = 0, int orderId = 0, string paymentMethodSystemName = null,
            OrderStatus? os = null,
            DateTime? orderStatusChangeFromUtc = null, DateTime? orderStatusChangeToUtc = null,
            PaymentStatus? ps = null, ShippingStatus? ss = null,
            DateTime? startTimeUtc = null, DateTime? endTimeUtc = null,
            string billingEmail = null, string billingLastName = "",
            bool ignoreCancelledOrders = false, string orderNotes = null);

        Customer GetCustomerByVendorEmail(string vendorEmail);
       #endregion

       #region Like
       void InsertLike(LikeInfoTable data);
       void DeleteLike(int id);
       void UpdateLike(int productId, int customerId, bool delete = false);
       void DeleteOldData(DateTime time);
       int GetCount(int productId);
       bool IsAvailable(int productId, int customerId);
       LikeInfoTable AvailbilityCheckWithData(int productId, int customerId);
       bool IsLikedByCustomer(int productId, int customerId);
       int GetLikeCountOfCustomer(int customerId);
       IPagedList<LikeInfoTable> GetLikedProductByCustomer(int customerId, int pageIndex = 0, int pageSize = int.MaxValue);
       IPagedList<BsLikeProductCount> MostLikedProduct(int pageIndex = 0, int pageSize = int.MaxValue);
       #endregion

       #region follow
       void InsertFollow(BsVendorFollowInfoTable data);
       void DeleteFollow(int id);
       void UpdateFollow(int vendoeId, int customerId, bool delete = false);
       int GetFollowCount(int vendoeId);
       bool IsAvailableVendor(int vendoeId, int customerId);
       BsVendorFollowInfoTable AvailbleVendorCheckWithData(int vendoeId, int customerId);
       bool IsFollowByCustomer(int vendoeId, int customerId);
       int GetFollowCountOfCustomer(int customerId);
       int GetFollowCountOfVendor(int vendorId);
       IPagedList<BsVendorFollowInfoTable> GetFollowVendorByCustomer(int customerId, int pageIndex = 0, int pageSize = int.MaxValue);
       IPagedList<BsVendorFollowInfoTable> GetFollowCustomerByVendorId(int vendorId, int pageIndex = 0, int pageSize = int.MaxValue);
       IPagedList<BsVendorFollowCount> MostFollowsVendor(int pageIndex = 0, int pageSize = int.MaxValue);
       #endregion

       #region Bs-Vendor-Notification

       BsVendorNotificationInfoTable GetNotificationById(int id);
       void InsertVendorNotification(BsVendorNotificationInfoTable data);
       void InsertVendorNotification(IEnumerable<BsVendorNotificationInfoTable> data);
       void UpdateVendorNotification(BsVendorNotificationInfoTable data);

       IPagedList<BsVendorNotificationInfoTable> GetUnViewNotificationByCustomerId(int customerId, int pageIndex = 0,
           int pageSize = int.MaxValue);

       IPagedList<BsVendorNotificationInfoTable> GetNotificationByCustomerId(int customerId, int pageIndex = 0,
           int pageSize = int.MaxValue);

       IPagedList<BsVendorNotificationInfoTable> GetAllNotificationByCustomerId(int customerId, int pageIndex = 0, int pageSize = int.MaxValue);
       int CountUnViewingNotificationByCustomerId(int customerId);
       IQueryable<BsVendorNotificationInfoTable> GetNotifications();
       IQueryable<OrderItem> GetOrderItemByOrderId(int orderId);
       IList<Customer> GetAllAdminCustomerByRollId(int[] customerRoleIds);
       CustomerRole GetAllAdminCustomerRole();

       #endregion

        #region Vendorpage

       int GetVendorProductCount(int vendorId);

       #endregion

    }
}
