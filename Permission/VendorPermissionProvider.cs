﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Security;
using Nop.Services.Security;

namespace Nop.Plugin.Bs.VendorSystem.Permission
{
    public class VendorPermissionProvider : IPermissionProvider
    {
        public static readonly PermissionRecord ManagePlugins = new PermissionRecord
        {
            Name = "Admin area. Manage Vendor Plugins",
            SystemName = "ManageVendorPlugins",
            Category = "Configuration"
        };

        public IEnumerable<PermissionRecord> GetPermissions()
        {
            return new[]
            {
                ManagePlugins
            };
        }

        public IEnumerable<DefaultPermissionRecord> GetDefaultPermissions()
        {
            return new[]
            {
                new DefaultPermissionRecord
                {
                    CustomerRoleSystemName = SystemCustomerRoleNames.Administrators,
                    PermissionRecords = new[]
                    {
                        ManagePlugins
                    }
                }
            };
        }
    }
}