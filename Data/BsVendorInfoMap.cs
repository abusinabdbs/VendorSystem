﻿using Nop.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Bs.VendorSystem.Domain;

namespace Nop.Plugin.Bs.VendorSystem.Data
{
    public class BsVendorInfoMap : NopEntityTypeConfiguration<BsVendorInfoTable>
    {
        public BsVendorInfoMap()
        {
            this.ToTable("BsVendorTable");
            this.HasKey(x => x.Id);
        }
    }
}
