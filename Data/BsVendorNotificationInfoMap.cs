﻿using Nop.Data.Mapping;
using Nop.Plugin.Bs.VendorSystem.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Data
{
    public class BsVendorNotificationInfoMap : NopEntityTypeConfiguration<BsVendorNotificationInfoTable>
    {
        public BsVendorNotificationInfoMap()
        {
            this.ToTable("BsVendorNotificationTable");
            this.HasKey(x => x.Id);
        }
    }
}
