﻿using Nop.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Bs.VendorSystem.Domain;

namespace Nop.Plugin.Bs.VendorSystem.Data
{
    public class BsVendorFollowInfoMap : NopEntityTypeConfiguration<BsVendorFollowInfoTable>
    {
        public BsVendorFollowInfoMap()
        {
            this.ToTable("BsVendorFollowTable");
            this.HasKey(x => x.Id);
        }
    }
}
