﻿using Nop.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Plugin.Bs.VendorSystem.Models;

namespace Nop.Plugin.Bs.VendorSystem.Data
{
    public class EfStartUp : IStartupTask
    {
        public void Execute()
        {
            //It's required to set initializer to null (for SQL Server Compact).
            //otherwise, you'll get something like "The model backing the 'your context name' context has changed since the database was created. Consider using Code First Migrations to update the database"
            Database.SetInitializer<BsVendorObjectContext>(null);

            Mapper.CreateMap<VendorRegisterModel, BsVendorInfoTable>();

            Mapper.CreateMap<BsVendorInfoTable, VendorRegisterModel>();
        }

        public int Order
        {
            //ensure that this task is run first 
            get { return 0; }
        }
    }
}
