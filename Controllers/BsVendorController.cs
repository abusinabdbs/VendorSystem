﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Core.Domain.Customers;
using Nop.Web.Infrastructure.Cache;
using Nop.Core.Caching;
using Nop.Services.Media;
using Nop.Web.Models.Media;
using Nop.Web.Models.Common;
using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Bs.VendorSystem.Domain;
using Nop.Plugin.Bs.VendorSystem.Extension;
using Nop.Plugin.Bs.VendorSystem.Models;
using Nop.Plugin.Bs.VendorSystem.Service;
using Nop.Services.Authentication;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Messages;
using Nop.Services.Vendors;
using Nop.Web.Framework.Kendoui;
using Nop.Admin.Models.Catalog;
using Nop.Services.Shipping;
using Nop.Services.Security;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework;
using Nop.Services.Directory;
using Nop.Core.Domain.Directory;
using Nop.Services.Helpers;
using Nop.Services.Tax;
using Nop.Services.Discounts;
using Nop.Core.Domain.Discounts;
using Nop.Admin.Extensions;
using Nop.Web.Framework.Mvc;
using Nop.Admin.Models.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Payments;
using Nop.Core.Domain.Tax;
using System.IO;
using System.Web.Routing;
using Nop.Admin.Controllers;
using Nop.Core.Domain.Media;
using Nop.Core.Infrastructure;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Catalog;


namespace Nop.Plugin.Bs.VendorSystem.Controllers
{
    public class BsVendorController : BasePluginController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IBsVendorService _bsVendorService;
        private readonly ICustomerService _customerService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IAddressService _addressService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly IVendorService _vendorService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly VendorSettings _vendorSettings;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILocalizationService _localizationService;

        private readonly IProductService _productService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly IPictureService _pictureService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IShippingService _shippingService;
        private readonly IPermissionService _permissionService;
        private readonly ILanguageService _languageService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IShipmentService _shipmentService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IDiscountService _discountService;
        private readonly IAclService _aclService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IProductTagService _productTagService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IDownloadService _downloadService;


        private readonly IOrderService _orderService;
        private readonly IOrderReportService _orderReportService;
        private readonly IPriceFormatter _priceFormatter;

        private readonly ICountryService _countryService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ShippingSettings _shippingSettings;

        private readonly AddressSettings _addressSettings;
        private readonly IGiftCardService _giftCardService;
        private readonly IEncryptionService _encryptionService;
        private readonly IPaymentService _paymentService;
        private readonly TaxSettings _taxSettings;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IReturnRequestService _returnRequestService;
        private readonly IPdfService _pdfService;

        private readonly ICacheManager _cacheManager;
        private readonly IWebHelper _webHelper;
        private readonly CatalogSettings _catalogSettings; 
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly MediaSettings _mediaSettings;
        public BsVendorController(IWorkContext workContext,
            IStoreContext storeContext,
            IBsVendorService bsVendorService,
            ICustomerService customerService,
            CustomerSettings customerSettings,
            ICustomerRegistrationService customerRegistrationService,
            IGenericAttributeService genericAttributeService,
            IAddressService addressService,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            IVendorService vendorService,
            IUrlRecordService urlRecordService,
            VendorSettings vendorSettings,
            IAuthenticationService authenticationService,
            ILocalizationService localizationService,
            IProductService productService,
            IProductTemplateService productTemplateService,
            AdminAreaSettings adminAreaSettings,
            IPictureService pictureService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IShippingService shippingService,
            IPermissionService permissionService,
            ILanguageService languageService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IMeasureService measureService,
            MeasureSettings measureSettings,
            IDateTimeHelper dateTimeHelper,
            IProductAttributeService productAttributeService,
            IShipmentService shipmentService,
            ITaxCategoryService taxCategoryService,
            ISpecificationAttributeService specificationAttributeService,
            IDiscountService discountService,
            IAclService aclService,
            ICustomerActivityService customerActivityService,
            ILocalizedEntityService localizedEntityService,
            IProductTagService productTagService,
            IShoppingCartService shoppingCartService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            IDownloadService downloadService,


            IOrderService orderService,
            IOrderReportService orderReportService,
            IPriceFormatter priceFormatter,


            ICountryService countryService,
            IOrderProcessingService orderProcessingService,
            ShippingSettings shippingSettings,

            AddressSettings addressSettings,
            IGiftCardService giftCardService,
            IEncryptionService encryptionService,
            IPaymentService paymentService,
            TaxSettings taxSettings,
            IReturnRequestService returnRequestService,
            IAddressAttributeFormatter addressAttributeFormatter,
            IPdfService pdfService,
            ICacheManager cacheManager,
            IWebHelper webhelper,
            CatalogSettings catalogSettings,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            MediaSettings mediaSettings)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._bsVendorService = bsVendorService;
            this._customerService = customerService;
            this._customerSettings = customerSettings;
            this._customerRegistrationService = customerRegistrationService;
            this._genericAttributeService = genericAttributeService;
            this._addressService = addressService;
            this._workflowMessageService = workflowMessageService;
            this._localizationSettings = localizationSettings;
            this._vendorService = vendorService;
            this._urlRecordService = urlRecordService;
            this._vendorSettings = vendorSettings;
            this._authenticationService = authenticationService;
            this._localizationService = localizationService;

            this._productService = productService;
            this._productTemplateService = productTemplateService;
            this._adminAreaSettings = adminAreaSettings;
            this._pictureService = pictureService;
            this._categoryService = categoryService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._shippingService = shippingService;
            this._permissionService = permissionService;
            this._languageService = languageService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._measureService = measureService;
            this._measureSettings = measureSettings;
            this._dateTimeHelper = dateTimeHelper;
            this._productAttributeService = productAttributeService;
            this._shipmentService = shipmentService;
            this._taxCategoryService = taxCategoryService;
            this._specificationAttributeService = specificationAttributeService;
            this._discountService = discountService;
            this._aclService = aclService;
            this._customerActivityService = customerActivityService;
            this._localizedEntityService = localizedEntityService;
            this._productTagService = productTagService;
            this._shoppingCartService = shoppingCartService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._downloadService = downloadService;


            this._orderService = orderService;
            this._giftCardService = giftCardService;
            this._encryptionService = encryptionService;
            this._orderReportService = orderReportService;
            this._priceFormatter = priceFormatter;

            this._countryService = countryService;
            this._orderProcessingService = orderProcessingService;
            this._shippingSettings = shippingSettings;

            this._addressSettings = addressSettings;
            this._paymentService = paymentService;
            this._giftCardService = giftCardService;
            this._encryptionService = encryptionService;
            this._taxSettings = taxSettings;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._returnRequestService = returnRequestService;
            this._pdfService = pdfService;

            this._cacheManager = cacheManager;
            this._webHelper = webhelper;
            this._catalogSettings = catalogSettings;
            this._priceCalculationService = priceCalculationService;
            this._taxService = taxService;
            this._mediaSettings = mediaSettings;
        }



        #region utility
        [NonAction]
        protected virtual bool CreateVendor(BsVendorInfoTable vendorThatWillBeApproved)
        {
            try
            {
                Customer customer = _customerService.GetCustomerById(vendorThatWillBeApproved.CustomerId);
                if (customer == null)
                    throw new ArgumentNullException("customer");

                //Create vendor
                var vendor = new Vendor();

                //prepare vendor entity
                vendor.Active = true;
                vendor.Email = vendorThatWillBeApproved.Email;
                vendor.Name = vendorThatWillBeApproved.FirstName+" "+vendorThatWillBeApproved.LastName;
                vendor.PageSize = 6;
                vendor.PageSizeOptions = _vendorSettings.DefaultVendorPageSizeOptions;
                _vendorService.InsertVendor(vendor);
                if (vendor.Id != 0)
                {
                    //search engine name
                    var vSeName = vendor.Name;
                    vSeName = vendor.ValidateSeName(vendorThatWillBeApproved.Url, vendor.Name, true);
                    _urlRecordService.SaveSlug(vendor, vSeName, 0);
                    customer.VendorId = vendor.Id;
                    customer.IsVendor(true);
                    vendorThatWillBeApproved.VendorId = vendor.Id;
                    _bsVendorService.Update(vendorThatWillBeApproved);
                    //vendor role
                    var vedorsRole = _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Vendors);
                    customer.CustomerRoles.Add(vedorsRole);
                    _customerService.UpdateCustomer(customer);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }
        #endregion

        #region DashBoard
        public ActionResult Index( )
        {


            if (!_workContext.CurrentCustomer.IsVendor())
                return new HttpUnauthorizedResult();
            //var vendorId = _workContext.CurrentCustomer.Id;

            var vendorId = _workContext.CurrentVendor.Id;
            if (vendorId== null)
            {
                return new HttpUnauthorizedResult();
            }
          
            DateTime vendorJoiningDate = _workContext.CurrentCustomer.CreatedOnUtc;
           // ViewBag.VendorJoiningDate = String.Format("{0:MMMM d, yyyy}", vendorJoiningDate);
            var vendorJoiningTotalTimes = (DateTime.UtcNow - _workContext.CurrentCustomer.CreatedOnUtc);
            var totalYear = (vendorJoiningTotalTimes.Days) / 365;
            var restOfmonth = ((vendorJoiningTotalTimes.Days) % 365) / 12;
            
           // ViewBag.TotalYearMonth = (totalYear + "yr" + " " + restOfmonth + "mths").ToString();

            #region Order Completed Total

            var model = new OrderListModel();
            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);
            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;
            var completeOrders = _orderService.SearchOrders(
                vendorId: vendorId,
                ps: PaymentStatus.Paid
               );
            decimal orderCompleteTotal = 0;


            foreach (var order in completeOrders)
            {
                var orderItemlst = order.OrderItems.Where(x => x.OrderId == order.Id).Select(y => new { y.Product, y.Quantity });
                foreach (var orderItem in orderItemlst)
                {
                    if (orderItem.Product.VendorId == vendorId)
                        orderCompleteTotal += (orderItem.Product.Price * orderItem.Quantity);
                }

            }

          //  ViewBag.OrderCompletedTotal = _priceFormatter.FormatPrice(orderCompleteTotal, true, false);
            #endregion

            #region Pending Order 
            var pendingOrders = _orderService.SearchOrders(
               vendorId: vendorId,
               ps: PaymentStatus.Pending
               );

            decimal orderPendingTotal = 0;
            decimal orderPendingQuintity = 0;


            foreach (var order in pendingOrders)
            {
                
              //  var productlst = order.OrderItems.Where(x => x.Product.VendorId == vendorId).Select(x=>x.Product);
                var orderItemlst = order.OrderItems.Where(x => x.OrderId == order.Id).Select(y => new{ y.Product, y.Quantity});
                foreach (var orderItem in orderItemlst)
                {
                    if (orderItem.Product.VendorId == vendorId)
                    {
                        orderPendingTotal += (orderItem.Product.Price * orderItem.Quantity);
                        orderPendingQuintity += orderItem.Quantity;
                    }
                        
                }
               
                

            }
         //   ViewBag.OrderPendingTotalQuintity = orderPendingQuintity.ToString();
           // ViewBag.OrderPendingTotal = _priceFormatter.FormatPrice(orderPendingTotal, true, false);
            #endregion

        
            //   var osComplete = _orderReportService.GetOrderAverageReportLine(os: OrderStatus.Complete, vendorId: vendorId, ignoreCancelledOrders: true);
            
           // decimal storderCompletedTotal = Convert.ToDecimal(osComplete.SumOrders);
           // ViewBag.OrderCompletedTotal = _priceFormatter.FormatPrice(orderCompleteTotal, true, false);
           // ViewBag.OrderCompleteTotalQuintity = osComplete.CountOrders.ToString();

           // var osPending = _orderReportService.GetOrderAverageReportLine(os: OrderStatus.Pending, vendorId: vendorId, ignoreCancelledOrders: true);
            //decimal storderPendingTotal = Convert.ToDecimal(osPending.SumOrders);
          //  ViewBag.OrderPendingTotal = _priceFormatter.FormatPrice(storderPendingTotal, true, false);
           // ViewBag.OrderPendingTotalQuintity = osPending.CountOrders.ToString();
           
            var model2 = new DashBoardModel();
            model2.OrderCompletedTotal = _priceFormatter.FormatPrice(orderCompleteTotal, true, false).ToString();
            model2.OrderPenddingTotal = _priceFormatter.FormatPrice(orderPendingTotal, true, false).ToString();
            model2.OrderPendingTotalQuintity = orderPendingQuintity.ToString();
            model2.VendorJoiningDate = String.Format("{0:MMMM d, yyyy}", vendorJoiningDate);
            model2.VendorCreateTotalYearMonth  = (totalYear + "yr" + " " + restOfmonth + "mths").ToString();
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Index.cshtml", model2);
        }
        public ActionResult BestsellersBriefReportByQuantity()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            // return PartialView();
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/BestsellersBriefReportByQuantity.cshtml");
        }
        [HttpPost]
        public ActionResult BestsellersBriefReportByQuantityList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            var quantitygridModel = GetBestsellersBriefReportModel(command.Page - 1,
                command.PageSize, 1);

            return Json(quantitygridModel);
        }
        public ActionResult BestsellersBriefReportByAmount()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            // return PartialView();
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/BestsellersBriefReportByAmount.cshtml");
        }
        [HttpPost]
        public ActionResult BestsellersBriefReportByAmountList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            var amountgridModel = GetBestsellersBriefReportModel(command.Page - 1,
                command.PageSize, 2);

            return Json(amountgridModel);
        }
        #endregion

        //[AdminAuthorize]
        //[ChildActionOnly]
        //public ActionResult Configure()
        //{
        //    return View("~/Plugins/Widgets.Like/Views/WidgetsLike/Configure.cshtml");
        //}
       // [AdminAuthorize]
        public ActionResult MarketPlace()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/MarketPlace.cshtml");
        }

        #region Vendor Registration
        public ActionResult VendorRegistration()
        {
            var model = new VendorRegisterModel();
           // model.CountryId = 186;
            PrepareVendorRegisterModel(model, false);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Registration.cshtml", model);
        }
        [HttpPost]
        public ActionResult VendorRegistration(VendorRegisterModel model)
        {

            var customer = _customerService.GetCustomerByEmail(model.Email);
            CustomerRegistrationResult registrationResult = new CustomerRegistrationResult();
            bool newCustomer = false;
            if (customer == null)
            {
                customer = _customerService.InsertGuestCustomer();
                if (ModelState.IsValid)
                {
                    bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
                    var registrationRequest = new CustomerRegistrationRequest(customer, model.Email,
                         model.Email, model.Password, _customerSettings.DefaultPasswordFormat, _storeContext.CurrentStore.Id, isApproved);
                    registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                    if (registrationResult.Success)
                    {
                        newCustomer = true;
                    }
                }
            }



            if (ModelState.IsValid)
            {



                if (registrationResult.Success)
                {

                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);


                    //insert default address (if possible)
                    var defaultAddress = new Address
                    {
                        FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                        LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                        Email = customer.Email,
                        Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                        CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                            (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                        StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                            (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                        Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),

                        PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),

                        CreatedOnUtc = customer.CreatedOnUtc
                    };
                    if (this._addressService.IsAddressValid(defaultAddress))
                    {
                        //some validation
                        if (defaultAddress.CountryId == 0)
                            defaultAddress.CountryId = null;
                        if (defaultAddress.StateProvinceId == 0)
                            defaultAddress.StateProvinceId = null;
                        //set default address
                        customer.Addresses.Add(defaultAddress);
                        customer.BillingAddress = defaultAddress;
                        customer.ShippingAddress = defaultAddress;
                        _customerService.UpdateCustomer(customer);
                    }

                    //notifications
                    if (newCustomer)
                    {
                        if (_customerSettings.NotifyNewCustomerRegistration)
                            _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);
                    }

                    var vendorInfoEntity = model.ToEntity();
                    vendorInfoEntity.CustomerId = customer.Id;
                    _bsVendorService.Insert(vendorInfoEntity);

                    switch (_customerSettings.UserRegistrationType)
                    {
                        case UserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                                //result
                                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.EmailValidation });
                            }
                        case UserRegistrationType.AdminApproval:
                            {
                                return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
                            }
                        case UserRegistrationType.Standard:
                            {
                                //send customer welcome message
                                _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                                var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });

                                return Redirect(redirectUrl);
                            }
                        default:
                            {
                                return RedirectToRoute("HomePage");
                            }
                    }
                }

                //errors
                foreach (var error in registrationResult.Errors)
                    ModelState.AddModelError("", error);
            }
            return View(model);
        }
       
        
        public ActionResult BsVendorEdit(int vendorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();

            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null || vendor.Deleted)
                return RedirectToAction("List");
            //var model = new BsVendorEditModel();
            var model = new VendorRegisterModel();
            model.VendorId = vendorId;
            PrepareVendorEditModel(model);
            var bsVendor = _bsVendorService.GetVendorByVendorId(vendorId);
            //if (bsVendor == null)
            //{
            //    var vendorInfoEntity = model.ToEntity();
            //    if(customer!=null)
            //     vendorInfoEntity.CustomerId = customer.Id;
            //    vendorInfoEntity.VendorId = vendorId;
            //    _bsVendorService.Insert(vendorInfoEntity);
            //    model = vendorInfoEntity.ToModel();

            //}
            //else
            //{
            //    model = bsVendor.ToModel();
            //}
            model = bsVendor.ToModel();
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/BsVendorEdit.cshtml", model);

        }

 
        [HttpPost,ActionName("BsVendorUpdate")]
        public ActionResult BsVendorUpdate(VendorRegisterModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                return AccessDeniedView();
            PrepareVendorEditModel(model);

            try
            {
                var bsVendor = _bsVendorService.GetVendorByVendorId(model.VendorId);

                bsVendor.AlternatePhoneNumber = model.AlternatePhoneNumber;
                bsVendor.StoreName = model.StoreName;
                bsVendor.BankName = model.BankName;
                bsVendor.BankAccountName = model.BankAccountName;
                bsVendor.BankAccountNumber = model.BankAccountNumber;
                bsVendor.Url = model.Url;
                bsVendor.Describe = model.Describe;
               
                _bsVendorService.Update(bsVendor);

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
          

        }

        protected virtual void PrepareVendorEditModel(VendorRegisterModel model)
        {
            var bsVendor = _bsVendorService.GetVendorByVendorId(model.VendorId);
            if (bsVendor == null)
            {
                var vendor = _vendorService.GetVendorById(model.VendorId);
                var customer = _customerService.GetCustomerByEmail(vendor.Email);
                var vendorInfoEntity = model.ToEntity();
                if (customer != null)
                vendorInfoEntity.CustomerId = customer.Id;
                vendorInfoEntity.VendorId = model.VendorId;
                vendorInfoEntity.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                vendorInfoEntity.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                vendorInfoEntity.Email = customer.Email;
                vendorInfoEntity.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
                vendorInfoEntity.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
                vendorInfoEntity.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
                vendorInfoEntity.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
                vendorInfoEntity.Approved = true;
                try
                {
                    _bsVendorService.Insert(vendorInfoEntity); 
                }
                catch(Exception ex)
                {
                    throw ex;
                }

                

              //  model = vendorInfoEntity.ToModel();
            }
            else
            {
              //  model = bsVendor.ToModel();
            }
        }
        [NonAction]
        protected virtual void PrepareVendorRegisterModel(VendorRegisterModel model, bool excludeProperties,
            string overrideCustomCustomerAttributesXml = "")
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });

            foreach (var c in _countryService.GetAllCountries(_workContext.WorkingLanguage.Id))
            {
                model.AvailableCountries.Add(new SelectListItem
                {
                    Text = c.GetLocalized(x => x.Name),
                    Value = c.Id.ToString(),
                    Selected = c.Id == model.CountryId
                });
            }

            bool anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

            model.AvailableStates.Add(new SelectListItem
            {
                Text = _localizationService.GetResource(anyCountrySelected ? "Address.OtherNonUS" : "Address.SelectState"),
                Value = "0"
            });

        }
        #endregion

        #region VendorApprove
        [AdminAuthorize]
        public ActionResult NonApprovedVendorList()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/NonApprovedVendorList.cshtml");
        }
        [AdminAuthorize]
        public ActionResult ReadAllVendorList(DataSourceRequest command)
        {
            var vendors = _bsVendorService.GetAllNotApprovedVendors(command.Page - 1, command.PageSize);
            var vendorModel = vendors
               .Select(x =>
               {
                   var gModel = new
                   {
                       Id = x.Id,
                       CustomerEmail = _customerService.GetCustomerById(x.CustomerId).Email,
                       VendorName = x.FirstName + " " + x.LastName,
                       VendorEmail = x.Email,
                       Description = x.Describe,
                       Phone = x.Phone,
                       Url = x.Url
                   };

                   return gModel;
               })
               .ToList();
            var gridModel = new DataSourceResult
            {
                Data = vendorModel,
                Total = vendors.Count
            };
            //a vendor should have access only to his products
            return Json(gridModel);
        }
        public ActionResult VendorApprove(int id)
        {
            var vendor = _bsVendorService.GetById(id);
            var check = CreateVendor(vendor);
            if (check == true)
            {
                vendor.Approved = true;
                _bsVendorService.Update(vendor);
            }
            return RedirectToAction("NonApprovedVendorList", "BsVendor");
        }
        #endregion

        #region Vendor Login
        public ActionResult VendorLogin()
        {
            var model = new VendorLoginModel();
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Login.cshtml", model);
        }
        [HttpPost]
        public ActionResult VendorLogin(VendorLoginModel model)
        {


            if (ModelState.IsValid)
            {

                var loginResult = _customerRegistrationService.ValidateCustomer(model.Email, model.Password);
                switch (loginResult)
                {
                    case CustomerLoginResults.Successful:
                        {
                            var customer = _customerService.GetCustomerByEmail(model.Email);

                            if (customer.IsVendor())
                            {
                                _authenticationService.SignIn(customer, false);
                                return Redirect("/vendor");
                               // return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Login.cshtml", model);
                            }

                            ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.VendorNotExist"));
                            break;

                        }
                    case CustomerLoginResults.CustomerNotExist:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                        break;
                    case CustomerLoginResults.Deleted:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                        break;
                    case CustomerLoginResults.NotActive:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                        break;
                    case CustomerLoginResults.NotRegistered:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                        break;
                    case CustomerLoginResults.WrongPassword:
                    default:
                        ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                        break;
                }
            }

            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Login.cshtml",model);
        }
        #endregion

        #region product list 
        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new ProductListModel();
               //model.DisplayProductPictures = _adminAreaSettings.DisplayProductPictures;
            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //stores
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var wh in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = wh.Name, Value = wh.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/List.cshtml", model);
        }
        [HttpPost]
        public ActionResult ProductList(DataSourceRequest command, ProductListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.SearchVendorId = _workContext.CurrentVendor.Id;
            }

            var categoryIds = new List<int> { model.SearchCategoryId };
            //include subcategories
            if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
                categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

            var products = _productService.SearchProducts(
                categoryIds: categoryIds,
                manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,
                warehouseId: model.SearchWarehouseId,
                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true,
                orderBy: ProductSortingEnum.Position
            );
            var gridModel = new DataSourceResult();
            gridModel.Data = products.Select(x =>
            {
                var productModel = x.ToModel();
                //little hack here:
                //ensure that product full descriptions are not returned
                //otherwise, we can get the following error if products have too long descriptions:
                //"Error during serialization or deserialization using the JSON JavaScriptSerializer. The length of the string exceeds the value set on the maxJsonLength property. "
                //also it improves performance
                productModel.FullDescription = "";

                //if (_adminAreaSettings.DisplayProductPictures)
                //{
                    var defaultProductPicture = _pictureService.GetPicturesByProductId(x.Id, 1).FirstOrDefault();
                    productModel.PictureThumbnailUrl = _pictureService.GetPictureUrl(defaultProductPicture, 75, true);
               // }
                productModel.ProductTypeName = x.ProductType.GetLocalizedEnum(_localizationService, _workContext);
                return productModel;
            });
            gridModel.Total = products.TotalCount;

            return Json(gridModel);
        }
        #endregion

        #region Product create
        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return  AccessDeniedView();

            var model = new VendorProductModel();
            PrepareProductModel(model, null, true, true);

            AddLocales(_languageService, model.ProductModel.Locales);
            PrepareAclModel(model, null, false);
            //PrepareStoresMappingModel(model, null, false);

            //model.ProductVariant = new ProductVariantModel();
            //model.ProductVariant.ParentProductId = 0;
            //PrepareProductModel(model.ProductVariant.Variant, null, true, true);

            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Create.cshtml", model);
        }
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(VendorProductModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                if (model.SelectedSecondSubCategoryId > 0)
                {
                    model.SelectedCategoryId = model.SelectedSecondSubCategoryId;
                }
                else
                {
                    if (model.SelectedFirstSubCategoryId > 0)
                    {
                        model.SelectedCategoryId = model.SelectedFirstSubCategoryId;
                    }
                    else
                        model.SelectedCategoryId = model.SelectedRootCategoryId;
                }


                //a vendor should have access only to his products
                if (_workContext.CurrentVendor != null)
                {
                    model.ProductModel.VendorId = _workContext.CurrentVendor.Id;
                }
                //vendors cannot edit "Show on home page" property
                if (_workContext.CurrentVendor != null && model.ProductModel.ShowOnHomePage)
                {
                    model.ProductModel.ShowOnHomePage = false;
                }
                //#region Product Publish Unpublis
                //if (_workContext.CurrentVendor != null)
                //{
                //    model.ProductModel.Published = false;

                //}
                //#endregion

                //product
                var product = model.ProductModel.ToEntity();
                //foreach (var stq in model.Options)
                //{
                //    product.StockQuantity += stq.Quantity;
                //}
                product.CreatedOnUtc = DateTime.UtcNow;
                product.UpdatedOnUtc = DateTime.UtcNow;

                //product.DealStatusId = (int)DealStatus.Pending;
                //product.SalesExecutiveId = _workContext.CurrentCustomer.Id;
                //product.DealVirtualEndDate = product.DealExpireDate;
                //product.HasThirdPartyCode = false;

                //product.InstallmentCount = (model.SelectedInstallmentIds != null) ? String.Join(",", model.SelectedInstallmentIds) : "1";

                _productService.InsertProduct(product);
                //search engine name
                model.ProductModel.SeName = product.ValidateSeName(model.ProductModel.SeName, product.Name, true).ToUrlString(true);
                _urlRecordService.SaveSlug(product, model.ProductModel.SeName, 0);
                //locales
                UpdateLocales(product, model);

                //ACL (customer roles)
                //SaveProductAcl(product, model);

                //Stores
                //SaveStoreMappings(product, model);

                //tags
                SaveProductTags(product, ParseProductTags(model.ProductModel.ProductTags));

                //warehouses
                //SaveProductWarehouseInventory(product, model);

                //discounts
                //var allDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToSkus, null, true);
                //foreach (var discount in allDiscounts)
                //{
                //    if (model.SelectedDiscountIds != null && model.SelectedDiscountIds.Contains(discount.Id))
                //        product.AppliedDiscounts.Add(discount);
                //}
                //_productService.UpdateProduct(product);
                //_productService.UpdateHasDiscountsApplied(product);


                //if (model.ProductVariant.CommissionRate > 84.74M)
                //    model.ProductVariant.CommissionRate = 84.74M;
                //if (product.ProductTypeId == (int)ProductType.SimpleProduct)
                //{
                //    var oldPfi = _productFinancialService.GetProductFianancialInfoByProductId(product.Id);
                //    if (oldPfi != null)
                //    {
                //       // oldPfi.CommissionRate = model.ProductVariant.CommissionRate * 1.18M;
                //        oldPfi.IsTaxIncluded = true;//model.ProductVariant.IsTaxIncluded;
                //        _productFinancialService.UpdateProductFianancialInfo(oldPfi);
                //    }
                //    else
                //    {
                //        ProductFinancialInfo newPfi = new ProductFinancialInfo();
                //        newPfi.CompanyId = product.VendorId;
                //        newPfi.PaymentTypeId = 1;
                //        newPfi.IsTaxIncluded = true;//model.ProductVariant.IsTaxIncluded;
                //       // newPfi.CommissionRate = model.ProductVariant.CommissionRate * 1.18M;

                //        _productFinancialService.InsertProductFianancialInfo(newPfi);
                //    }
                //}


                #region giftolyo


                #region save giftolyo pictures added by sohel

                SaveOrEditProductPictures(product, model);

                #endregion

                #region product categories
                SaveOrEditProductCategories(product, model);

                #endregion
                #endregion
                #region product attribute
                // save product Attribute
                int productSiseAttributeMappingId = CreateProductSizeAttributeMapping(product, model);

                SaveProductSizeAttributeValues(product, model, productSiseAttributeMappingId);

                int productColorAttributeMappingId = CreateProductColorAttributeMapping(product, model);

                SaveProductColorAttributeValues(product, model, productColorAttributeMappingId);
                #endregion
                //activity log
                _customerActivityService.InsertActivity("AddNewProduct", _localizationService.GetResource("ActivityLog.AddNewProduct"), product.Name);
                if (_workContext.CurrentVendor != null)
                {
                  // return RedirectToRoute("Vendor", new { seName = _workContext.CurrentVendor.GetSeName() });
                    //return RedirectToAction("Edit", new { id = product.Id });
                    RedirectToAction("List", "BsVendor");
                }

               

               // SuccessNotification(_localizationService.GetResource("Admin.Catalog.Products.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = product.Id }) : RedirectToAction("List","BsVendor");
            }

            //If we got this far, something failed, redisplay form
            PrepareProductModel(model, null, false, true);
            //PrepareProductModel(model, null, true, true);
            PrepareAclModel(model, null, true);
            //PrepareStoresMappingModel(model, null, true);
            //model.ProductVariant = new ProductVariantModel();
            //model.ProductVariant.ParentProductId = 0;
            //PrepareProductModel(model.ProductVariant.Variant, null, true, true);
            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Create.cshtml", model);
        }
        #endregion

        #region Product Edit
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var product = _productService.GetProductById(id);
            if (product == null || product.Deleted)
                //No product found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List");

          //  var model = product.ToModel();
            var model = new VendorProductModel();
            model.ProductModel.Id = id;

            PrepareProductModel(model, product, false, false);
            AddLocales(_languageService, model.ProductModel.Locales, (locale, languageId) =>
            {
                locale.Name = product.GetLocalized(x => x.Name, languageId, false, false);
                locale.ShortDescription = product.GetLocalized(x => x.ShortDescription, languageId, false, false);
                locale.FullDescription = product.GetLocalized(x => x.FullDescription, languageId, false, false);
                locale.MetaKeywords = product.GetLocalized(x => x.MetaKeywords, languageId, false, false);
                locale.MetaDescription = product.GetLocalized(x => x.MetaDescription, languageId, false, false);
                locale.MetaTitle = product.GetLocalized(x => x.MetaTitle, languageId, false, false);
                locale.SeName = product.GetSeName(languageId, false, false);
            });

            PrepareAclModel(model, product, false);
            //model.ProductVariant = new ProductVariantModel();
            //model.ProductVariant.ParentProductId = product.Id;
            //var pfi = _productFinancialService.GetProductFianancialInfoByProductId(product.Id);
            //if (pfi != null)
            //{
            //    model.ProductVariant.IsTaxIncluded = pfi.IsTaxIncluded;
            //    model.ProductVariant.CommissionRate = pfi.CommissionRate;
            //}
            //PrepareProductModel(model.ProductVariant.Variant, null, true, true);
            //PrepareStoresMappingModel(model, product, false);
           // return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Edit.cshtml", model);
        }
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(VendorProductModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var product = _productService.GetProductById(model.ProductModel.Id);
            if (product == null || product.Deleted)
                //No product found with the specified id
                return RedirectToAction("List", "BsVendor");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List", "BsVendor");


            if (ModelState.IsValid)
            {
                if (model.SelectedSecondSubCategoryId > 0)
                {
                    model.SelectedCategoryId = model.SelectedSecondSubCategoryId;
                }
                else
                {
                    if (model.SelectedFirstSubCategoryId > 0)
                    {
                        model.SelectedCategoryId = model.SelectedFirstSubCategoryId;
                    }
                    else
                        model.SelectedCategoryId = model.SelectedRootCategoryId;
                }
                //a vendor should have access only to his products
                if (_workContext.CurrentVendor != null)
                {
                    model.ProductModel.VendorId = _workContext.CurrentVendor.Id;
                }
                //vendors cannot edit "Show on home page" property
                if (_workContext.CurrentVendor != null && model.ProductModel.ShowOnHomePage != product.ShowOnHomePage)
                {
                    model.ProductModel.ShowOnHomePage = product.ShowOnHomePage;
                }
                var prevStockQuantity = product.GetTotalStockQuantity();
                //#region Product Publish Unpublis
                //if (_workContext.CurrentVendor != null)
                //{
                //    model.ProductModel.Published = false;

                //}
                //#endregion
                //product
                //var neuProduct = product;
               // product = model.ProductModel.ToEntity(product);

                product.UpdatedOnUtc = DateTime.UtcNow;

                _productService.UpdateProduct(product);

                //search engine name
                model.ProductModel.SeName = product.ValidateSeName(model.ProductModel.SeName, product.Name, true).ToUrlString(true);
                _urlRecordService.SaveSlug(product, model.ProductModel.SeName, 0);

                UpdatePictureSeoNames(product);

                #region Giftolyo added by sohel


                #region Update giftolyo pictures

                SaveOrEditProductPictures(product, model);

                #endregion


                #region product categories
                SaveOrEditProductCategories(product, model);

                #endregion
                #endregion


                //activity log
                _customerActivityService.InsertActivity("EditProduct", _localizationService.GetResource("ActivityLog.EditProduct"), product.Name);

              //  SuccessNotification(_localizationService.GetResource("Admin.Catalog.Products.Updated"));

                if (_workContext.CurrentVendor != null)
                {
                    return RedirectToRoute(new
                    {
                        controller = "BsVendor",
                        action = "List"
                    });
                    //return RedirectToRoute("Vendor", new { seName = _workContext.CurrentVendor.GetSeName() });
                }

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabIndex();

                    //return RedirectToAction("Edit", new { id = product.Id });
                    return RedirectToAction("List", "BsVendor");
                }
                return RedirectToAction("List", "BsVendor");
            }

            //If we got this far, something failed, redisplay form
            PrepareProductModel(model, product, false, true);
            //model.ProductVariant = new ProductVariantModel();
            //model.ProductVariant.ParentProductId = product.Id;
            //PrepareProductModel(model.ProductVariant.Variant, null, true, true);
            //var pfi = _productFinancialService.GetProductFianancialInfoByProductId(product.Id);
            //if (pfi != null)
            //{
            //    model.ProductVariant.IsTaxIncluded = pfi.IsTaxIncluded;
            //    model.ProductVariant.CommissionRate = pfi.CommissionRate;
            //}

            PrepareAclModel(model, product, true);
            //PrepareStoresMappingModel(model, product, true);
            return View(model);
        }
        #endregion

        #region Prduct Attribute
        [HttpPost]    
        public ActionResult ProductAttributeValueList(int productAttributeMappingId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeMappingId);
            if (productAttributeMapping == null)
                throw new ArgumentException("No product attribute mapping found with the specified id");

            var product = _productService.GetProductById(productAttributeMapping.ProductId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return Content("This is not your product");

            var values = _productAttributeService.GetProductAttributeValues(productAttributeMappingId);
            //var values2 = _productAttributeService.GetProductAttributeValueById()
            var gridModel = new DataSourceResult
            {
                Data = values.Select(x =>
                {
                    Product associatedProduct = null;
                    if (x.AttributeValueType == AttributeValueType.AssociatedToProduct)
                    {
                        associatedProduct = _productService.GetProductById(x.AssociatedProductId);
                    }
                    var pictureThumbnailUrl = _pictureService.GetPictureUrl(x.PictureId, 75, false);
                    //little hack here. Grid is rendered wrong way with <inmg> without "src" attribute
                    if (String.IsNullOrEmpty(pictureThumbnailUrl))
                        pictureThumbnailUrl = _pictureService.GetPictureUrl(null, 1, true);
                    return new ProductModel.ProductAttributeValueModel
                    {
                        Id = x.Id,
                        ProductAttributeMappingId = x.ProductAttributeMappingId,
                        AttributeValueTypeId = x.AttributeValueTypeId,
                        AttributeValueTypeName = x.AttributeValueType.GetLocalizedEnum(_localizationService, _workContext),
                        AssociatedProductId = x.AssociatedProductId,
                        AssociatedProductName = associatedProduct != null ? associatedProduct.Name : "",
                        Name = x.ProductAttributeMapping.AttributeControlType != AttributeControlType.ColorSquares ? x.Name : string.Format("{0} - {1}", x.Name, x.ColorSquaresRgb),
                        ColorSquaresRgb = x.ColorSquaresRgb,
                        PriceAdjustment = x.PriceAdjustment,
                        PriceAdjustmentStr = x.AttributeValueType == AttributeValueType.Simple ? x.PriceAdjustment.ToString("G29") : "",
                        WeightAdjustment = x.WeightAdjustment,
                        WeightAdjustmentStr = x.AttributeValueType == AttributeValueType.Simple ? x.WeightAdjustment.ToString("G29") : "",
                        Cost = x.Cost,
                        Quantity = x.Quantity,
                        IsPreSelected = x.IsPreSelected,
                        DisplayOrder = x.DisplayOrder,
                        PictureId = x.PictureId,
                        PictureThumbnailUrl = pictureThumbnailUrl
                    };
                }),
                Total = values.Count()
            };

            return Json(gridModel);
        }
        public ActionResult ProductColorAttributeValueList(int productAttributeMappingId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeMappingId);
            if (productAttributeMapping == null)
                throw new ArgumentException("No product attribute mapping found with the specified id");

            var product = _productService.GetProductById(productAttributeMapping.ProductId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return Content("This is not your product");

            var values = _productAttributeService.GetProductAttributeValues(productAttributeMappingId);

            var gridModel = new DataSourceResult
            {
                Data = values.Select(x =>
                {
                    Product associatedProduct = null;
                    if (x.AttributeValueType == AttributeValueType.AssociatedToProduct)
                    {
                        associatedProduct = _productService.GetProductById(x.AssociatedProductId);
                    }
                    var pictureThumbnailUrl = _pictureService.GetPictureUrl(x.PictureId, 75, false);
                    //little hack here. Grid is rendered wrong way with <inmg> without "src" attribute
                    if (String.IsNullOrEmpty(pictureThumbnailUrl))
                        pictureThumbnailUrl = _pictureService.GetPictureUrl(null, 1, true);
                    return new 
                    {
                        ColorId = x.Id,
                        ColorName = x.ProductAttributeMapping.AttributeControlType != AttributeControlType.ColorSquares ? x.Name : string.Format("{0} - {1}", x.Name, x.ColorSquaresRgb),
                        ColorQuantity = x.Quantity,
                        
                    };
                }),
                Total = values.Count()
            };

            return Json(gridModel);
        }
        public ActionResult ProductAttributeValueDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var pav = _productAttributeService.GetProductAttributeValueById(id);
            if (pav == null)
                throw new ArgumentException("No product attribute value found with the specified id");

            var product = _productService.GetProductById(pav.ProductAttributeMapping.ProductId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return Content("This is not your product");

            _productAttributeService.DeleteProductAttributeValue(pav);

            return new NullJsonResult();
        }
        public ActionResult ProductColorAttributeValueDelete(int colorId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var pav = _productAttributeService.GetProductAttributeValueById(colorId);
            if (pav == null)
                throw new ArgumentException("No product attribute value found with the specified id");

            var product = _productService.GetProductById(pav.ProductAttributeMapping.ProductId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return Content("This is not your product");

            _productAttributeService.DeleteProductAttributeValue(pav);

            return new NullJsonResult();
        }
        public ActionResult ProductAttributeValueCreate(FormCollection form)
        {
            try
            {


                var pav = new ProductAttributeValue
                {
                    ProductAttributeMappingId = Int32.Parse(form["ProductAttributeMappingId"]),
                    Name = form["Name"],
                    Quantity = Int32.Parse(form["Quantity"]),

                };

                _productAttributeService.InsertProductAttributeValue(pav);

                return Json(new
                {

                    success = 1

                });

            }
            catch (Exception exc)
            {

                return Json(new { error = 1, message = exc.Message });
            }


        }
        public ActionResult ProductColorAttributeValueCreate(FormCollection form)
        {
            try
            {


                var pav = new ProductAttributeValue
                {
                    ProductAttributeMappingId = Int32.Parse(form["ProductAttributeMappingId"]),
                    Name = form["Name"],
                    Quantity = Int32.Parse(form["Quantity"]),

                };

                _productAttributeService.InsertProductAttributeValue(pav);

                return Json(new
                {

                    success = 1

                });

            }
            catch (Exception exc)
            {

                return Json(new { error = 1, message = exc.Message });
            }


        }
        #endregion

        #region Attribute Mapping popup
        public ActionResult AddAttributeCombinationPopup(string btnId, string formId, int productId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var product = _productService.GetProductById(productId);
            if (product == null)
                //No product found with the specified id
                return RedirectToAction("List", "Product");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List", "Product");

            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            var model = new  AddProductAttributeCombinationModel();
            PrepareAddProductAttributeCombinationModel(model, product);
            
            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/AddAttributeCombinationPopup.cshtml", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddAttributeCombinationPopup(string btnId, string formId, int productId,
            AddProductAttributeCombinationModel model, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var product = _productService.GetProductById(productId);
            if (product == null)
                //No product found with the specified id
                return RedirectToAction("List", "Product");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List", "Product");

            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            //attributes
            string attributesXml = "";
            var warnings = new List<string>();

            #region Product attributes

            var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id)
                //ignore non-combinable attributes for combinations
                .Where(x => !x.IsNonCombinable())
                .ToList();
            foreach (var attribute in attributes)
            {
                string controlId = string.Format("product_attribute_{0}", attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.ColorSquares:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    //case AttributeControlType.Checkboxes:
                    //    {
                    //        var cblAttributes = form[controlId];
                    //        if (!String.IsNullOrEmpty(cblAttributes))
                    //        {
                    //            foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    //            {
                    //                int selectedAttributeId = int.Parse(item);
                    //                if (selectedAttributeId > 0)
                    //                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                    //                        attribute, selectedAttributeId.ToString());
                    //            }
                    //        }
                    //    }
                    //    break;
                    //case AttributeControlType.ReadonlyCheckboxes:
                    //    {
                    //        //load read-only (already server-side selected) values
                    //        var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                    //        foreach (var selectedAttributeId in attributeValues
                    //            .Where(v => v.IsPreSelected)
                    //            .Select(v => v.Id)
                    //            .ToList())
                    //        {
                    //            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                    //                attribute, selectedAttributeId.ToString());
                    //        }
                    //    }
                    //    break;
                    //case AttributeControlType.TextBox:
                    //case AttributeControlType.MultilineTextbox:
                    //    {
                    //        var ctrlAttributes = form[controlId];
                    //        if (!String.IsNullOrEmpty(ctrlAttributes))
                    //        {
                    //            string enteredText = ctrlAttributes.Trim();
                    //            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                    //                attribute, enteredText);
                    //        }
                    //    }
                    //    break;
                    //case AttributeControlType.Datepicker:
                    //    {
                    //        var date = form[controlId + "_day"];
                    //        var month = form[controlId + "_month"];
                    //        var year = form[controlId + "_year"];
                    //        DateTime? selectedDate = null;
                    //        try
                    //        {
                    //            selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(date));
                    //        }
                    //        catch { }
                    //        if (selectedDate.HasValue)
                    //        {
                    //            attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                    //                attribute, selectedDate.Value.ToString("D"));
                    //        }
                    //    }
                    //    break;
                    //case AttributeControlType.FileUpload:
                    //    {
                    //        var httpPostedFile = this.Request.Files[controlId];
                    //        if ((httpPostedFile != null) && (!String.IsNullOrEmpty(httpPostedFile.FileName)))
                    //        {
                    //            var fileSizeOk = true;
                    //            if (attribute.ValidationFileMaximumSize.HasValue)
                    //            {
                    //                //compare in bytes
                    //                var maxFileSizeBytes = attribute.ValidationFileMaximumSize.Value * 1024;
                    //                if (httpPostedFile.ContentLength > maxFileSizeBytes)
                    //                {
                    //                    warnings.Add(string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), attribute.ValidationFileMaximumSize.Value));
                    //                    fileSizeOk = false;
                    //                }
                    //            }
                    //            if (fileSizeOk)
                    //            {
                    //                //save an uploaded file
                    //                var download = new Download
                    //                {
                    //                    DownloadGuid = Guid.NewGuid(),
                    //                    UseDownloadUrl = false,
                    //                    DownloadUrl = "",
                    //                    DownloadBinary = httpPostedFile.GetDownloadBits(),
                    //                    ContentType = httpPostedFile.ContentType,
                    //                    Filename = Path.GetFileNameWithoutExtension(httpPostedFile.FileName),
                    //                    Extension = Path.GetExtension(httpPostedFile.FileName),
                    //                    IsNew = true
                    //                };
                    //                _downloadService.InsertDownload(download);
                    //                //save attribute
                    //                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                    //                    attribute, download.DownloadGuid.ToString());
                    //            }
                    //        }
                    //    }
                       // break;
                    default:
                        break;
                }
            }
            //validate conditional attributes (if specified)
            foreach (var attribute in attributes)
            {
                var conditionMet = _productAttributeParser.IsConditionMet(attribute, attributesXml);
                if (conditionMet.HasValue && !conditionMet.Value)
                {
                    attributesXml = _productAttributeParser.RemoveProductAttribute(attributesXml, attribute);
                }
            }

            #endregion

            warnings.AddRange(_shoppingCartService.GetShoppingCartItemAttributeWarnings(_workContext.CurrentCustomer,
                ShoppingCartType.ShoppingCart, product, 1, attributesXml, true));
            if (warnings.Count == 0)
            {
                //save combination
                var combination = new ProductAttributeCombination
                {
                    ProductId = product.Id,
                    AttributesXml = attributesXml,
                    StockQuantity = model.StockQuantity,
                    AllowOutOfStockOrders = model.AllowOutOfStockOrders,
                    Sku = model.Sku,
                    ManufacturerPartNumber = model.ManufacturerPartNumber,
                    Gtin = model.Gtin,
                    OverriddenPrice = model.OverriddenPrice,
                    NotifyAdminForQuantityBelow = model.NotifyAdminForQuantityBelow,
                };
                _productAttributeService.InsertProductAttributeCombination(combination);

                ViewBag.RefreshPage = true;
               // return View(model);
                return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/AddAttributeCombinationPopup.cshtml", model);
            }

            //If we got this far, something failed, redisplay form
            PrepareAddProductAttributeCombinationModel(model, product);
            model.Warnings = warnings;
            return View(model);
        }
        protected virtual void PrepareAddProductAttributeCombinationModel(AddProductAttributeCombinationModel model, Product product)
        {
            if (model == null)
                throw new ArgumentNullException("model");
            if (product == null)
                throw new ArgumentNullException("product");

            model.ProductId = product.Id;
            model.StockQuantity = 10000;
            model.NotifyAdminForQuantityBelow = 1;

            var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id)
                //ignore non-combinable attributes for combinations
                .Where(x => !x.IsNonCombinable())
                .ToList();
            foreach (var attribute in attributes)
            {
                var attributeModel = new AddProductAttributeCombinationModel.ProductAttributeModel
                {
                    Id = attribute.Id,
                    ProductAttributeId = attribute.ProductAttributeId,
                    Name = attribute.ProductAttribute.Name,
                    TextPrompt = attribute.TextPrompt,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new AddProductAttributeCombinationModel.ProductAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }

                model.ProductAttributes.Add(attributeModel);
            }
        }
        public ActionResult ProductAttributeCombinationList(DataSourceRequest command, int productId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return Content("This is not your product");

            var combinations = _productAttributeService.GetAllProductAttributeCombinations(productId);
            var combinationsModel = combinations
                .Select(x =>
                {
                    var pacModel = new ProductModel.ProductAttributeCombinationModel
                    {
                        Id = x.Id,
                        ProductId = x.ProductId,
                        AttributesXml = _productAttributeFormatter.FormatAttributes(x.Product, x.AttributesXml, _workContext.CurrentCustomer, "<br />", true, true, true, false),
                        StockQuantity = x.StockQuantity,
                        AllowOutOfStockOrders = x.AllowOutOfStockOrders,
                        Sku = x.Sku,
                        ManufacturerPartNumber = x.ManufacturerPartNumber,
                        Gtin = x.Gtin,
                        OverriddenPrice = x.OverriddenPrice,
                        NotifyAdminForQuantityBelow = x.NotifyAdminForQuantityBelow
                    };
                    //warnings
                    var warnings = _shoppingCartService.GetShoppingCartItemAttributeWarnings(_workContext.CurrentCustomer,
                        ShoppingCartType.ShoppingCart, x.Product, 1, x.AttributesXml, true);
                    for (int i = 0; i < warnings.Count; i++)
                    {
                        pacModel.Warnings += warnings[i];
                        if (i != warnings.Count - 1)
                            pacModel.Warnings += "<br />";
                    }

                    return pacModel;
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = combinationsModel,
                Total = combinationsModel.Count
            };

            return Json(gridModel);
        }
        [HttpPost]
        public ActionResult ProductAttributeCombinationDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var combination = _productAttributeService.GetProductAttributeCombinationById(id);
            if (combination == null)
                throw new ArgumentException("No product attribute combination found with the specified id");

            var product = _productService.GetProductById(combination.ProductId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return Content("This is not your product");

            _productAttributeService.DeleteProductAttributeCombination(combination);

            return new NullJsonResult();
        }
        #endregion

        #region SaveTabeindex
        protected void SaveSelectedTabIndex(int? index = null, bool persistForTheNextRequest = true)
        {
            //keep this method synchronized with
            //"GetSelectedTabIndex" method of \Nop.Web.Framework\ViewEngines\Razor\WebViewPage.cs
            if (!index.HasValue)
            {
                int tmp;
                if (int.TryParse(this.Request.Form["selected-tab-index"], out tmp))
                {
                    index = tmp;
                }
            }
            if (index.HasValue)
            {
                string dataKey = "nop.selected-tab-index";
                if (persistForTheNextRequest)
                {
                    TempData[dataKey] = index;
                }
                else
                {
                    ViewData[dataKey] = index;
                }
            }
        }
        #endregion

        #region Bestseller Report
        [NonAction]
        protected DataSourceResult GetBestsellersBriefReportModel(int pageIndex,
            int pageSize, int orderBy)
        {
            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            var items = _orderReportService.BestSellersReport(
                vendorId: vendorId,
                orderBy: orderBy,
                pageIndex: pageIndex,
                pageSize: pageSize,
                showHidden: true);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var m = new BestsellersReportLineModel
                    {
                        ProductId = x.ProductId,
                        TotalAmount = _priceFormatter.FormatPrice(x.TotalAmount, true, false),
                        TotalQuantity = x.TotalQuantity,
                    };
                    var product = _productService.GetProductById(x.ProductId);
                    if (product != null)
                        m.ProductName = product.Name;
                    return m;
                }),
                Total = items.TotalCount
            };
            return gridModel;
        }
        public ActionResult BestsellersReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new BestsellersReportModel();

            //order statuses
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //billing countries
            foreach (var c in _countryService.GetAllCountriesForBilling(showHidden: true))
            {
                model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            }
            model.AvailableCountries.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //vendor
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

           /// return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/BestsellersReport.cshtml", model);
        }
        [HttpPost]
        public ActionResult BestsellersReportList(DataSourceRequest command, BestsellersReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;

            var items = _orderReportService.BestSellersReport(
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                billingCountryId: model.BillingCountryId,
                orderBy: 2,
                vendorId: vendorId,
                categoryId: model.CategoryId,
                manufacturerId: model.ManufacturerId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var m = new BestsellersReportLineModel
                    {
                        ProductId = x.ProductId,
                        TotalAmount = _priceFormatter.FormatPrice(x.TotalAmount, true, false),
                        TotalQuantity = x.TotalQuantity,
                    };
                    var product = _productService.GetProductById(x.ProductId);
                    if (product != null)
                        m.ProductName = product.Name;
                    return m;
                }),
                Total = items.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Never SoldOut Product 
        public ActionResult NeverSoldReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new NeverSoldReportModel();
            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/NeverSoldReport.cshtml", model);
        }
        [HttpPost]
        public ActionResult NeverSoldReportList(DataSourceRequest command, NeverSoldReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var items = _orderReportService.ProductsNeverSold(vendorId,
                startDateValue, endDateValue,
                command.Page - 1, command.PageSize, true);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                    new NeverSoldReportLineModel
                    {
                        ProductId = x.Id,
                        ProductName = x.Name,
                    }),
                Total = items.TotalCount
            };

            return Json(gridModel);
        }
        #endregion 

        #region No Action Methode
        [NonAction]
        protected virtual void PrepareOrderDetailsModel(OrderModel model, Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = order.Id;
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.OrderStatusId = order.OrderStatusId;
            model.OrderGuid = order.OrderGuid;
            var store = _storeService.GetStoreById(order.StoreId);
            model.StoreName = store != null ? store.Name : "Unknown";
            model.CustomerId = order.CustomerId;
            var customer = order.Customer;
            model.CustomerInfo = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.CustomerIp = order.CustomerIp;
            model.VatNumber = order.VatNumber;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.AllowCustomersToSelectTaxDisplayType = _taxSettings.AllowCustomersToSelectTaxDisplayType;
            model.TaxDisplayType = _taxSettings.TaxDisplayType;
            model.AffiliateId = order.AffiliateId;
            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
            //custom values
            model.CustomValues = order.DeserializeCustomValues();

            #region Order totals

            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if (primaryStoreCurrency == null)
                throw new Exception("Cannot load primary store currency");

            //subtotal
            model.OrderSubtotalInclTax = _priceFormatter.FormatPrice(order.OrderSubtotalInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            model.OrderSubtotalExclTax = _priceFormatter.FormatPrice(order.OrderSubtotalExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            model.OrderSubtotalInclTaxValue = order.OrderSubtotalInclTax;
            model.OrderSubtotalExclTaxValue = order.OrderSubtotalExclTax;
            //discount (applied to order subtotal)
            string orderSubtotalDiscountInclTaxStr = _priceFormatter.FormatPrice(order.OrderSubTotalDiscountInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            string orderSubtotalDiscountExclTaxStr = _priceFormatter.FormatPrice(order.OrderSubTotalDiscountExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            if (order.OrderSubTotalDiscountInclTax > decimal.Zero)
                model.OrderSubTotalDiscountInclTax = orderSubtotalDiscountInclTaxStr;
            if (order.OrderSubTotalDiscountExclTax > decimal.Zero)
                model.OrderSubTotalDiscountExclTax = orderSubtotalDiscountExclTaxStr;
            model.OrderSubTotalDiscountInclTaxValue = order.OrderSubTotalDiscountInclTax;
            model.OrderSubTotalDiscountExclTaxValue = order.OrderSubTotalDiscountExclTax;

            //shipping
            model.OrderShippingInclTax = _priceFormatter.FormatShippingPrice(order.OrderShippingInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            model.OrderShippingExclTax = _priceFormatter.FormatShippingPrice(order.OrderShippingExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            model.OrderShippingInclTaxValue = order.OrderShippingInclTax;
            model.OrderShippingExclTaxValue = order.OrderShippingExclTax;

            //payment method additional fee
            if (order.PaymentMethodAdditionalFeeInclTax > decimal.Zero)
            {
                model.PaymentMethodAdditionalFeeInclTax = _priceFormatter.FormatPaymentMethodAdditionalFee(order.PaymentMethodAdditionalFeeInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
                model.PaymentMethodAdditionalFeeExclTax = _priceFormatter.FormatPaymentMethodAdditionalFee(order.PaymentMethodAdditionalFeeExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            }
            model.PaymentMethodAdditionalFeeInclTaxValue = order.PaymentMethodAdditionalFeeInclTax;
            model.PaymentMethodAdditionalFeeExclTaxValue = order.PaymentMethodAdditionalFeeExclTax;


            //tax
            model.Tax = _priceFormatter.FormatPrice(order.OrderTax, true, false);
            SortedDictionary<decimal, decimal> taxRates = order.TaxRatesDictionary;
            bool displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
            bool displayTax = !displayTaxRates;
            foreach (var tr in order.TaxRatesDictionary)
            {
                model.TaxRates.Add(new OrderModel.TaxRate
                {
                    Rate = _priceFormatter.FormatTaxRate(tr.Key),
                    Value = _priceFormatter.FormatPrice(tr.Value, true, false),
                });
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.TaxValue = order.OrderTax;
            model.TaxRatesValue = order.TaxRates;

            //discount
            if (order.OrderDiscount > 0)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-order.OrderDiscount, true, false);
            model.OrderTotalDiscountValue = order.OrderDiscount;

            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-gcuh.UsedValue, true, false),
                });
            }

            //reward points
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-order.RedeemedRewardPointsEntry.UsedAmount, true, false);
            }

            //total
            model.OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false);
            model.OrderTotalValue = order.OrderTotal;

            //refunded amount
            if (order.RefundedAmount > decimal.Zero)
                model.RefundedAmount = _priceFormatter.FormatPrice(order.RefundedAmount, true, false);

            //used discounts
            var duh = _discountService.GetAllDiscountUsageHistory(null, null, order.Id, 0, int.MaxValue);
            foreach (var d in duh)
            {
                model.UsedDiscounts.Add(new OrderModel.UsedDiscountModel
                {
                    DiscountId = d.DiscountId,
                    DiscountName = d.Discount.Name
                });
            }

            #endregion

            #region Payment info

            if (order.AllowStoringCreditCardNumber)
            {
                //card type
                model.CardType = _encryptionService.DecryptText(order.CardType);
                //cardholder name
                model.CardName = _encryptionService.DecryptText(order.CardName);
                //card number
                model.CardNumber = _encryptionService.DecryptText(order.CardNumber);
                //cvv
                model.CardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                //expiry date
                string cardExpirationMonthDecrypted = _encryptionService.DecryptText(order.CardExpirationMonth);
                if (!String.IsNullOrEmpty(cardExpirationMonthDecrypted) && cardExpirationMonthDecrypted != "0")
                    model.CardExpirationMonth = cardExpirationMonthDecrypted;
                string cardExpirationYearDecrypted = _encryptionService.DecryptText(order.CardExpirationYear);
                if (!String.IsNullOrEmpty(cardExpirationYearDecrypted) && cardExpirationYearDecrypted != "0")
                    model.CardExpirationYear = cardExpirationYearDecrypted;

                model.AllowStoringCreditCardNumber = true;
            }
            else
            {
                string maskedCreditCardNumberDecrypted = _encryptionService.DecryptText(order.MaskedCreditCardNumber);
                if (!String.IsNullOrEmpty(maskedCreditCardNumberDecrypted))
                    model.CardNumber = maskedCreditCardNumberDecrypted;
            }


            //payment transaction info
            model.AuthorizationTransactionId = order.AuthorizationTransactionId;
            model.CaptureTransactionId = order.CaptureTransactionId;
            model.SubscriptionTransactionId = order.SubscriptionTransactionId;

            //payment method info
            var pm = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = pm != null ? pm.PluginDescriptor.FriendlyName : order.PaymentMethodSystemName;
            model.PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);

            //payment method buttons
            model.CanCancelOrder = _orderProcessingService.CanCancelOrder(order);
            model.CanCapture = _orderProcessingService.CanCapture(order);
            model.CanMarkOrderAsPaid = _orderProcessingService.CanMarkOrderAsPaid(order);
            model.CanRefund = _orderProcessingService.CanRefund(order);
            model.CanRefundOffline = _orderProcessingService.CanRefundOffline(order);
            model.CanPartiallyRefund = _orderProcessingService.CanPartiallyRefund(order, decimal.Zero);
            model.CanPartiallyRefundOffline = _orderProcessingService.CanPartiallyRefundOffline(order, decimal.Zero);
            model.CanVoid = _orderProcessingService.CanVoid(order);
            model.CanVoidOffline = _orderProcessingService.CanVoidOffline(order);

            model.PrimaryStoreCurrencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode;
            model.MaxAmountToRefund = order.OrderTotal - order.RefundedAmount;

            //recurring payment record
            var recurringPayment = _orderService.SearchRecurringPayments(0, 0, order.Id, null, 0, int.MaxValue, true).FirstOrDefault();
            if (recurringPayment != null)
            {
                model.RecurringPaymentId = recurringPayment.Id;
            }
            #endregion

            #region Billing & shipping info

            model.BillingAddress = order.BillingAddress.ToModel();
            model.BillingAddress.FormattedCustomAddressAttributes = _addressAttributeFormatter.FormatAttributes(order.BillingAddress.CustomAttributes);
            model.BillingAddress.FirstNameEnabled = true;
            model.BillingAddress.FirstNameRequired = true;
            model.BillingAddress.LastNameEnabled = true;
            model.BillingAddress.LastNameRequired = true;
            model.BillingAddress.EmailEnabled = true;
            model.BillingAddress.EmailRequired = true;
            model.BillingAddress.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.BillingAddress.CompanyRequired = _addressSettings.CompanyRequired;
            model.BillingAddress.CountryEnabled = _addressSettings.CountryEnabled;
            model.BillingAddress.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.BillingAddress.CityEnabled = _addressSettings.CityEnabled;
            model.BillingAddress.CityRequired = _addressSettings.CityRequired;
            model.BillingAddress.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.BillingAddress.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.BillingAddress.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.BillingAddress.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.BillingAddress.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.BillingAddress.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.BillingAddress.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.BillingAddress.PhoneRequired = _addressSettings.PhoneRequired;
            model.BillingAddress.FaxEnabled = _addressSettings.FaxEnabled;
            model.BillingAddress.FaxRequired = _addressSettings.FaxRequired;

            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext); ;
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;

                model.PickUpInStore = order.PickUpInStore;
                if (!order.PickUpInStore)
                {
                    model.ShippingAddress = order.ShippingAddress.ToModel();
                    model.ShippingAddress.FormattedCustomAddressAttributes = _addressAttributeFormatter.FormatAttributes(order.ShippingAddress.CustomAttributes);
                    model.ShippingAddress.FirstNameEnabled = true;
                    model.ShippingAddress.FirstNameRequired = true;
                    model.ShippingAddress.LastNameEnabled = true;
                    model.ShippingAddress.LastNameRequired = true;
                    model.ShippingAddress.EmailEnabled = true;
                    model.ShippingAddress.EmailRequired = true;
                    model.ShippingAddress.CompanyEnabled = _addressSettings.CompanyEnabled;
                    model.ShippingAddress.CompanyRequired = _addressSettings.CompanyRequired;
                    model.ShippingAddress.CountryEnabled = _addressSettings.CountryEnabled;
                    model.ShippingAddress.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
                    model.ShippingAddress.CityEnabled = _addressSettings.CityEnabled;
                    model.ShippingAddress.CityRequired = _addressSettings.CityRequired;
                    model.ShippingAddress.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
                    model.ShippingAddress.StreetAddressRequired = _addressSettings.StreetAddressRequired;
                    model.ShippingAddress.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
                    model.ShippingAddress.StreetAddress2Required = _addressSettings.StreetAddress2Required;
                    model.ShippingAddress.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
                    model.ShippingAddress.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
                    model.ShippingAddress.PhoneEnabled = _addressSettings.PhoneEnabled;
                    model.ShippingAddress.PhoneRequired = _addressSettings.PhoneRequired;
                    model.ShippingAddress.FaxEnabled = _addressSettings.FaxEnabled;
                    model.ShippingAddress.FaxRequired = _addressSettings.FaxRequired;

                    model.ShippingAddressGoogleMapsUrl = string.Format("http://maps.google.com/maps?f=q&hl=en&ie=UTF8&oe=UTF8&geocode=&q={0}", Server.UrlEncode(order.ShippingAddress.Address1 + " " + order.ShippingAddress.ZipPostalCode + " " + order.ShippingAddress.City + " " + (order.ShippingAddress.Country != null ? order.ShippingAddress.Country.Name : "")));
                }
                model.ShippingMethod = order.ShippingMethod;

                model.CanAddNewShipments = order.HasItemsToAddToShipment();
                
            }

            #endregion

            #region Products
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;
            bool hasDownloadableItems = false;
            var products = order.OrderItems;
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                products = products
                    .Where(orderItem => orderItem.Product.VendorId == _workContext.CurrentVendor.Id)
                    .ToList();
            }
            foreach (var orderItem in products)
            {
                if (orderItem.Product.IsDownload)
                    hasDownloadableItems = true;

                var orderItemModel = new OrderModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    ProductId = orderItem.ProductId,
                    ProductName = orderItem.Product.Name,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    Quantity = orderItem.Quantity,
                    IsDownload = orderItem.Product.IsDownload,
                    DownloadCount = orderItem.DownloadCount,
                    DownloadActivationType = orderItem.Product.DownloadActivationType,
                    IsDownloadActivated = orderItem.IsDownloadActivated
                };
                //license file
                if (orderItem.LicenseDownloadId.HasValue)
                {
                    var licenseDownload = _downloadService.GetDownloadById(orderItem.LicenseDownloadId.Value);
                    if (licenseDownload != null)
                    {
                        orderItemModel.LicenseDownloadGuid = licenseDownload.DownloadGuid;
                    }
                }
                //vendor
                var vendor = _vendorService.GetVendorById(orderItem.Product.VendorId);
                orderItemModel.VendorName = vendor != null ? vendor.Name : "";

                //unit price
                orderItemModel.UnitPriceInclTaxValue = orderItem.UnitPriceInclTax;
                orderItemModel.UnitPriceExclTaxValue = orderItem.UnitPriceExclTax;
                orderItemModel.UnitPriceInclTax = _priceFormatter.FormatPrice(orderItem.UnitPriceInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                orderItemModel.UnitPriceExclTax = _priceFormatter.FormatPrice(orderItem.UnitPriceExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);
                //discounts
                orderItemModel.DiscountInclTaxValue = orderItem.DiscountAmountInclTax;
                orderItemModel.DiscountExclTaxValue = orderItem.DiscountAmountExclTax;
                orderItemModel.DiscountInclTax = _priceFormatter.FormatPrice(orderItem.DiscountAmountInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                orderItemModel.DiscountExclTax = _priceFormatter.FormatPrice(orderItem.DiscountAmountExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);
                //subtotal
                orderItemModel.SubTotalInclTaxValue = orderItem.PriceInclTax;
                orderItemModel.SubTotalExclTaxValue = orderItem.PriceExclTax;
                orderItemModel.SubTotalInclTax = _priceFormatter.FormatPrice(orderItem.PriceInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                orderItemModel.SubTotalExclTax = _priceFormatter.FormatPrice(orderItem.PriceExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);

                orderItemModel.AttributeInfo = orderItem.AttributeDescription;
                if (orderItem.Product.IsRecurring)
                    orderItemModel.RecurringInfo = string.Format(_localizationService.GetResource("Admin.Orders.Products.RecurringPeriod"), orderItem.Product.RecurringCycleLength, orderItem.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }

                //return requests
               // orderItemModel.ReturnRequestIds = _orderService.SearchReturnRequests(0, 0, orderItem.Id, null, 0, int.MaxValue)
                   // .Select(rr => rr.Id).ToList();
                orderItemModel.ReturnRequestIds = _returnRequestService.SearchReturnRequests(orderItemId: orderItem.Id)
                   .Select(rr => rr.Id).ToList();
                //gift cards
                orderItemModel.PurchasedGiftCardIds = _giftCardService.GetGiftCardsByPurchasedWithOrderItemId(orderItem.Id)
                    .Select(gc => gc.Id).ToList();

                model.Items.Add(orderItemModel);
            }
            model.HasDownloadableProducts = hasDownloadableItems;
            #endregion
        }
        [NonAction]
        protected virtual bool HasAccessToOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var vendorId = _workContext.CurrentVendor.Id;
            var hasVendorProducts = order.OrderItems.Any(orderItem => orderItem.Product.VendorId == vendorId);
            return hasVendorProducts;
        }
        [NonAction]
        protected virtual bool HasAccessToOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException("orderItem");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var vendorId = _workContext.CurrentVendor.Id;
            return orderItem.Product.VendorId == vendorId;
        }
        [NonAction]
        protected virtual bool HasAccessToProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var vendorId = _workContext.CurrentVendor.Id;
            return product.VendorId == vendorId;
        }
        [NonAction]
        protected virtual bool HasAccessToShipment(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var hasVendorProducts = false;
            var vendorId = _workContext.CurrentVendor.Id;
            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem != null)
                {
                    if (orderItem.Product.VendorId == vendorId)
                    {
                        hasVendorProducts = true;
                        break;
                    }
                }
            }
            return hasVendorProducts;
        }
        [NonAction]
        protected virtual ShipmentModel PrepareShipmentModel(Shipment shipment, bool prepareProducts, bool prepareShipmentEvent = false)
        {
            //measures
            var baseWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            var baseWeightIn = baseWeight != null ? baseWeight.Name : "";
            var baseDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var baseDimensionIn = baseDimension != null ? baseDimension.Name : "";

            var model = new ShipmentModel
            {
                Id = shipment.Id,
                OrderId = shipment.OrderId,
                TrackingNumber = shipment.TrackingNumber,
                TotalWeight = shipment.TotalWeight.HasValue ? string.Format("{0:F2} [{1}]", shipment.TotalWeight, baseWeightIn) : "",
                ShippedDate = shipment.ShippedDateUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc).ToString() : _localizationService.GetResource("Admin.Orders.Shipments.ShippedDate.NotYet"),
                ShippedDateUtc = shipment.ShippedDateUtc,
                CanShip = !shipment.ShippedDateUtc.HasValue,
                DeliveryDate = shipment.DeliveryDateUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc).ToString() : _localizationService.GetResource("Admin.Orders.Shipments.DeliveryDate.NotYet"),
                DeliveryDateUtc = shipment.DeliveryDateUtc,
                CanDeliver = shipment.ShippedDateUtc.HasValue && !shipment.DeliveryDateUtc.HasValue,
                AdminComment = shipment.AdminComment,
            };

            if (prepareProducts)
            {
                foreach (var shipmentItem in shipment.ShipmentItems)
                {
                    var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                    if (orderItem == null)
                        continue;

                    //quantities
                    var qtyInThisShipment = shipmentItem.Quantity;
                    var maxQtyToAdd = orderItem.GetTotalNumberOfItemsCanBeAddedToShipment();
                    var qtyOrdered = orderItem.Quantity;
                    var qtyInAllShipments = orderItem.GetTotalNumberOfItemsInAllShipment();

                    var warehouse = _shippingService.GetWarehouseById(shipmentItem.WarehouseId);
                    var shipmentItemModel = new ShipmentModel.ShipmentItemModel
                    {
                        Id = shipmentItem.Id,
                        OrderItemId = orderItem.Id,
                        ProductId = orderItem.ProductId,
                        ProductName = orderItem.Product.Name,
                        Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                        AttributeInfo = orderItem.AttributeDescription,
                        ShippedFromWarehouse = warehouse != null ? warehouse.Name : null,
                        ShipSeparately = orderItem.Product.ShipSeparately,
                        ItemWeight = orderItem.ItemWeight.HasValue ? string.Format("{0:F2} [{1}]", orderItem.ItemWeight, baseWeightIn) : "",
                        ItemDimensions = string.Format("{0:F2} x {1:F2} x {2:F2} [{3}]", orderItem.Product.Length, orderItem.Product.Width, orderItem.Product.Height, baseDimensionIn),
                        QuantityOrdered = qtyOrdered,
                        QuantityInThisShipment = qtyInThisShipment,
                        QuantityInAllShipments = qtyInAllShipments,
                        QuantityToAdd = maxQtyToAdd,
                    };
                    //rental info
                    if (orderItem.Product.IsRental)
                    {
                        var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                        var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                        shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                            rentalStartDate, rentalEndDate);
                    }

                    model.Items.Add(shipmentItemModel);
                }
            }

            if (prepareShipmentEvent && !String.IsNullOrEmpty(shipment.TrackingNumber))
            {
                var order = shipment.Order;
                var srcm = _shippingService.LoadShippingRateComputationMethodBySystemName(order.ShippingRateComputationMethodSystemName);
                if (srcm != null &&
                    srcm.PluginDescriptor.Installed &&
                    srcm.IsShippingRateComputationMethodActive(_shippingSettings))
                {
                    var shipmentTracker = srcm.ShipmentTracker;
                    if (shipmentTracker != null)
                    {
                        model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
                        if (_shippingSettings.DisplayShipmentEventsToStoreOwner)
                        {
                            var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
                            if (shipmentEvents != null)
                            {
                                foreach (var shipmentEvent in shipmentEvents)
                                {
                                    var shipmentStatusEventModel = new ShipmentModel.ShipmentStatusEventModel();
                                    var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
                                    shipmentStatusEventModel.Country = shipmentEventCountry != null
                                        ? shipmentEventCountry.GetLocalized(x => x.Name)
                                        : shipmentEvent.CountryCode;
                                    shipmentStatusEventModel.Date = shipmentEvent.Date;
                                    shipmentStatusEventModel.EventName = shipmentEvent.EventName;
                                    shipmentStatusEventModel.Location = shipmentEvent.Location;
                                    model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
                                }
                            }
                        }
                    }
                }
            }

            return model;
        }
        [NonAction]
        protected virtual void UpdatePictureSeoNames(Product product)
        {
            foreach (var pp in product.ProductPictures)
                _pictureService.SetSeoFilename(pp.PictureId, _pictureService.GetPictureSeName(product.Name));
        }
        [NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }
        [NonAction]
        protected virtual void SaveProductTags(Product product, string[] productTags)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            //product tags
            var existingProductTags = product.ProductTags.ToList();
            var productTagsToRemove = new List<ProductTag>();
            foreach (var existingProductTag in existingProductTags)
            {
                bool found = false;
                foreach (string newProductTag in productTags)
                {
                    if (existingProductTag.Name.Equals(newProductTag, StringComparison.InvariantCultureIgnoreCase))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    productTagsToRemove.Add(existingProductTag);
                }
            }
            foreach (var productTag in productTagsToRemove)
            {
                product.ProductTags.Remove(productTag);
                _productService.UpdateProduct(product);
            }
            foreach (string productTagName in productTags)
            {
                ProductTag productTag;
                var productTag2 = _productTagService.GetProductTagByName(productTagName);
                if (productTag2 == null)
                {
                    //add new product tag
                    productTag = new ProductTag
                    {
                        Name = productTagName
                    };
                    _productTagService.InsertProductTag(productTag);
                }
                else
                {
                    productTag = productTag2;
                }
                if (!product.ProductTagExists(productTag.Id))
                {
                    product.ProductTags.Add(productTag);
                    _productService.UpdateProduct(product);
                }
            }
        }
        [NonAction]
        protected virtual void SaveOrEditProductPictures(Product product, VendorProductModel model)
        {
           // var oldProductPictures = _productService.GetProductPicturesByProductId(product.Id);
            var oldProductPictures = _bsVendorService.GetProductPicturesByProductId(product.Id);
            var oldSelectedPictureIds = oldProductPictures.Select(x => x.PictureId).ToArray();

            var selectedPictureIds = new List<int>();
            model.PictureIds = PrepareProductPictureIds(model);
            if (model.PictureIds != null)
                selectedPictureIds = model.PictureIds.ToList();
            IEnumerable<int> needDeletePictureIds = oldSelectedPictureIds.Except(selectedPictureIds);
            IEnumerable<int> needAddPictureIds = selectedPictureIds.Except(oldSelectedPictureIds.ToList());
            //Insert category mapping
            foreach (var pictureId in needAddPictureIds)
            {
                InsertProductPicture(product.Id, pictureId);
            }
            //delete product pictures and mapping
            foreach (var pictureId in needDeletePictureIds)
            {
                var productPicture = oldProductPictures.FindProductPicture(product.Id, pictureId);
                DeleteProductPicture(productPicture);
                var picture = _pictureService.GetPictureById(pictureId);
                _pictureService.DeletePicture(picture);
            }
        }
        [NonAction]
        protected virtual void PrepareProductModel(VendorProductModel model, Product product,
            bool setPredefinedValues, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (product != null)
            {
                model.ProductModel.Name = product.Name;
                model.ProductModel.StockQuantity = product.StockQuantity;
                model.ProductModel.Price = product.Price;
                model.ProductModel.ShortDescription = product.ShortDescription;
                model.ProductModel.FullDescription = product.FullDescription;
                var parentGroupedProduct = _productService.GetProductById(product.ParentGroupedProductId);
                if (parentGroupedProduct != null)
                {
                    model.ProductModel.AssociatedToProductId = product.ParentGroupedProductId;
                    model.ProductModel.AssociatedToProductName = parentGroupedProduct.Name;
                }
            }

            model.ProductModel.PrimaryStoreCurrencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode;
            model.ProductModel.BaseWeightIn = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId).Name;
            model.ProductModel.BaseDimensionIn = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId).Name;
            if (product != null)
            {
                model.ProductModel.CreatedOn = _dateTimeHelper.ConvertToUserTime(product.CreatedOnUtc, DateTimeKind.Utc);
                model.ProductModel.UpdatedOn = _dateTimeHelper.ConvertToUserTime(product.UpdatedOnUtc, DateTimeKind.Utc);
            }

            //little performance hack here
            //there's no need to load attributes, categories, manufacturers when creating a new product
            //anyway they're not used (you need to save a product before you map add them)
            if (product != null)
            {
                foreach (var productAttribute in _productAttributeService.GetAllProductAttributes())
                {
                    model.ProductModel.AvailableProductAttributes.Add(new SelectListItem
                    {
                        Text = productAttribute.Name,
                        Value = productAttribute.Id.ToString()
                    });
                }
                foreach (var manufacturer in _manufacturerService.GetAllManufacturers(showHidden: true))
                {
                    model.ProductModel.AvailableManufacturers.Add(new SelectListItem
                    {
                        Text = manufacturer.Name,
                        Value = manufacturer.Id.ToString()
                    });
                }

            }

            //copy product
            if (product != null)
            {
                model.ProductModel.CopyProductModel.Id = product.Id;
                model.ProductModel.CopyProductModel.Name = "Copy of " + product.Name;
                model.ProductModel.CopyProductModel.Published = true;
                model.ProductModel.CopyProductModel.CopyImages = true;
            }

            //templates
            var templates = _productTemplateService.GetAllProductTemplates();
            foreach (var template in templates)
            {
                model.ProductModel.AvailableProductTemplates.Add(new SelectListItem
                {
                    Text = template.Name,
                    Value = template.Id.ToString()
                });
            }

            //vendors
            model.ProductModel.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
            model.ProductModel.AvailableVendors.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Admin.Catalog.Products.Fields.Vendor.None"),
                Value = "0"
            });
            var vendors = _vendorService.GetAllVendors(showHidden: true);
            foreach (var vendor in vendors)
            {
                model.ProductModel.AvailableVendors.Add(new SelectListItem
                {
                    Text = vendor.Name,
                    Value = vendor.Id.ToString()
                });
            }


            //sales executives
            //var allSalesExecutives = _bsVendorService.GetCustomersByRoleId(8);
            //foreach (var salesExecutive in allSalesExecutives)
            //{
            //    model.ProductModel.AvailableSalesExecutives.Add(new SelectListItem
            //    {
            //        Text = salesExecutive.GetFullName(),
            //        Value = salesExecutive.Id.ToString()
            //    });
            //}

            #region selected category
            if (product != null)
            {
                var selectedProductCategory = product.ProductCategories.FirstOrDefault();               
                var selectedFirstSubCategry = selectedProductCategory != null ? _categoryService.GetCategoryById(selectedProductCategory.Category.ParentCategoryId) : null;               
                var selectedRootCategry = selectedFirstSubCategry != null ? _categoryService.GetCategoryById(selectedFirstSubCategry.ParentCategoryId) : null;
                if (selectedRootCategry  !=null)
                {
                    model.SelectedSecondSubCategoryId = selectedProductCategory != null ? selectedProductCategory.CategoryId : 0;
                    model.SelectedFirstSubCategoryId = selectedFirstSubCategry != null ? selectedFirstSubCategry.Id : 0;
                    model.SelectedRootCategoryId = selectedRootCategry != null ? selectedRootCategry.Id : 0;
                }
                else
                {
                    if (selectedFirstSubCategry != null)
                    {
                        model.SelectedFirstSubCategoryId = selectedProductCategory != null ? selectedProductCategory.CategoryId : 0;
                        model.SelectedRootCategoryId = selectedFirstSubCategry != null ? selectedFirstSubCategry.Id : 0;
                    }
                    else
                    {
                        model.SelectedRootCategoryId = selectedProductCategory != null ? selectedProductCategory.CategoryId : 0;
                    }
                }

                
            }

            #endregion

            var allCategories = _categoryService.GetAllCategories(showHidden: true);
 
            foreach (var category in allCategories)
            {
                model.ProductModel.AvailableCategories.Add(new SelectListItem
                {
                    Text = category.GetFormattedBreadCrumb(_categoryService),
                    Value = category.Id.ToString()
                });
            
               
            }

        #region RootCategory
            var availableRootCategories = _categoryService.GetAllCategories(showHidden: true);
            model.AvailableRootCategories.Add(new SelectListItem
            {
                Text = "Select One",
                Value = "0"           
            });
            foreach (var category in availableRootCategories)
                    {
                     if (category.ParentCategoryId > 0) continue;
                       model.AvailableRootCategories.Add(new SelectListItem{
                           Text = category.Name,
                           Value =category.Id.ToString(),
                           Selected=category.Id==model.SelectedRootCategoryId
                       });
                       
                     }
            
                   var availableFirstSubCategories = _categoryService.GetAllCategoriesByParentCategoryId(model.SelectedRootCategoryId, showHidden: true);
                   model.AvailableFirstSubCategories.Add(new SelectListItem
                   {
                       Text = "Select One",
                       Value = "0"
                   });
                   foreach (var category in availableFirstSubCategories)
                   {                      
                       model.AvailableFirstSubCategories.Add(new SelectListItem
                       {
                           Text = category.Name,
                           Value = category.Id.ToString(),
                           Selected = category.Id == model.SelectedFirstSubCategoryId
                       });
                   }

                   var availableSecondSubCategories = _categoryService.GetAllCategoriesByParentCategoryId(model.SelectedFirstSubCategoryId, showHidden: true);
                   model.AvailableSecondSubCategories.Add(new SelectListItem
                   {
                       Text = "Select One",
                       Value = "0"
                   });
                   foreach (var category in availableSecondSubCategories)
                   {
                    
                       model.AvailableSecondSubCategories.Add(new SelectListItem
                       {
                           Text = category.Name,
                           Value = category.Id.ToString(),
                           Selected = category.Id == model.SelectedSecondSubCategoryId
                       });
                   }
                
        #endregion


                   //delivery dates
            model.ProductModel.AvailableDeliveryDates.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Admin.Catalog.Products.Fields.DeliveryDate.None"),
                Value = "0"
            });
            var deliveryDates = _shippingService.GetAllDeliveryDates();
            foreach (var deliveryDate in deliveryDates)
            {
                model.ProductModel.AvailableDeliveryDates.Add(new SelectListItem
                {
                    Text = deliveryDate.Name,
                    Value = deliveryDate.Id.ToString()
                });
            }

            //warehouses
            var warehouses = _shippingService.GetAllWarehouses();
            model.ProductModel.AvailableWarehouses.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Admin.Catalog.Products.Fields.Warehouse.None"),
                Value = "0"
            });
            foreach (var warehouse in warehouses)
            {
                model.ProductModel.AvailableWarehouses.Add(new SelectListItem
                {
                    Text = warehouse.Name,
                    Value = warehouse.Id.ToString()
                });
            }

            //multiple warehouses
            foreach (var warehouse in warehouses)
            {
                var pwiModel = new ProductModel.ProductWarehouseInventoryModel
                {
                    WarehouseId = warehouse.Id,
                    WarehouseName = warehouse.Name
                };
                if (product != null)
                {
                    var pwi = product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == warehouse.Id);
                    if (pwi != null)
                    {
                        pwiModel.WarehouseUsed = true;
                        pwiModel.StockQuantity = pwi.StockQuantity;
                        pwiModel.ReservedQuantity = pwi.ReservedQuantity;
                        pwiModel.PlannedQuantity = _shipmentService.GetQuantityInShipments(product, pwi.WarehouseId, true, true);
                    }
                }
                model.ProductModel.ProductWarehouseInventoryModels.Add(pwiModel);
            }

            //product tags
            if (product != null)
            {
                var result = new StringBuilder();
                for (int i = 0; i < product.ProductTags.Count; i++)
                {
                    var pt = product.ProductTags.ToList()[i];
                    result.Append(pt.Name);
                    if (i != product.ProductTags.Count - 1)
                        result.Append(", ");
                }
                model.ProductModel.ProductTags = result.ToString();
            }

            //tax categories
            var taxCategories = _taxCategoryService.GetAllTaxCategories();
            model.ProductModel.AvailableTaxCategories.Add(new SelectListItem { Text = "---", Value = "0" });
            foreach (var tc in taxCategories)
                model.ProductModel.AvailableTaxCategories.Add(new SelectListItem { Text = tc.Name, Value = tc.Id.ToString(), Selected = product != null && !setPredefinedValues && tc.Id == product.TaxCategoryId });

            //specification attributes
            var specificationAttributes = _specificationAttributeService.GetSpecificationAttributes();
            model.ProductModel.AddSpecificationAttributeModel.AvailableOptions.Add(new SelectListItem
            {
                Text = "Select One",
                Value = "0"
            });
            for (int i = 0; i < specificationAttributes.Count; i++)
            {
                var sa = specificationAttributes[i];
                model.ProductModel.AddSpecificationAttributeModel.AvailableAttributes.Add(new SelectListItem { Text = sa.Name, Value = sa.Id.ToString() });
                if (i == 0)
                {
                    //attribute options
                    foreach (var sao in _specificationAttributeService.GetSpecificationAttributeOptionsBySpecificationAttribute(sa.Id))
                        model.ProductModel.AddSpecificationAttributeModel.AvailableOptions.Add(new SelectListItem { Text = sao.Name, Value = sao.Id.ToString() });
                }
            }
            //default specs values
            model.ProductModel.AddSpecificationAttributeModel.ShowOnProductPage = true;

            //discounts
            model.ProductModel.AvailableDiscounts = _discountService
                .GetAllDiscounts(DiscountType.AssignedToSkus, showHidden: true)
                .Select(d => d.ToModel())
                .ToList();
            if (!excludeProperties && product != null)
            {
                model.ProductModel.SelectedDiscountIds = product.AppliedDiscounts.Select(d => d.Id).ToArray();
            }


            //installments
            //model.ProductModel.AvailableInstallments = new List<ProductModel.InstallmentModel>();
            //for (int i = 1; i < 10; i++)
            //{
            //    model.ProductModel.AvailableInstallments.Add(new ProductModel.InstallmentModel
            //    {
            //        Id = i,
            //        Name = i == 1 ? "Tek Çekim" : i.ToString() + " Taksit",
            //        Count = i
            //    });

            //}

            //if (product != null)
            //{
            //    model.ProductModel.SelectedInstallmentIds = product.ParseInstallmentCounts();
            //}
            //else
            //{
            //    var tekCekim = new List<int> { 1 };
            //    model.ProductModel.SelectedInstallmentIds = tekCekim.ToArray();
            //}



            //default values
            if (setPredefinedValues)
            {

                SetPredefinedValues(model.ProductModel);



            }

            #region giftolyo product category and pictures added by sohel

            if (product != null)
            {
                #region product categories
                var productCategories = _categoryService.GetProductCategoriesByProductId(product.Id, true);
                if (productCategories != null && productCategories.Count > 0)
                {
                    model.SelectedCategoryId = productCategories.FirstOrDefault().CategoryId;
                }


                #endregion

                var productPictures = _pictureService.GetPicturesByProductId(product.Id);
                if (productPictures != null && productPictures.Count > 0)
                {
                    switch (productPictures.Count)
                    {
                        case 1:
                            model.PictureId1 = productPictures.FirstOrDefault().Id;
                            break;
                        case 2: model.PictureId1 = productPictures[0].Id;
                            model.PictureId2 = productPictures[1].Id;
                            break;
                        case 3: model.PictureId1 = productPictures[0].Id;
                            model.PictureId2 = productPictures[1].Id;
                            model.PictureId3 = productPictures[2].Id;
                            break;
                        case 4:
                            model.PictureId1 = productPictures[0].Id;
                            model.PictureId2 = productPictures[1].Id;
                            model.PictureId3 = productPictures[2].Id;
                            model.PictureId4 = productPictures[3].Id;
                            break;
                        default:
                            model.PictureId1 = productPictures[0].Id;
                            model.PictureId2 = productPictures[1].Id;
                            model.PictureId3 = productPictures[2].Id;
                            model.PictureId4 = productPictures[3].Id;
                            break;
                    }
                }
            }



            #endregion


        }
        [NonAction]
        protected virtual string[] ParseProductTags(string productTags)
        {
            var result = new List<string>();
            if (!String.IsNullOrWhiteSpace(productTags))
            {
                string[] values = productTags.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string val1 in values)
                    if (!String.IsNullOrEmpty(val1.Trim()))
                        result.Add(val1.Trim());
            }
            return result.ToArray();
        }
        [NonAction]
        protected virtual void InsertProductPicture(int productId, int pictureId)
        {
            if (pictureId == 0)
                throw new ArgumentException();

            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");


           // var existingProductPictures = _productService.GetProductPicturesByProductId(productId);
            var existingProductPictures =  _bsVendorService.GetProductPicturesByProductId(product.Id);
            if (existingProductPictures.FindProductPicture(productId, pictureId) == null)
            {
                _productService.InsertProductPicture(new ProductPicture
                {
                    PictureId = pictureId,
                    ProductId = productId,
                    DisplayOrder = 0,
                });

                _pictureService.SetSeoFilename(pictureId, _pictureService.GetPictureSeName(product.Name));
            }

        }
        [NonAction]
        protected virtual void SaveOrEditProductCategories(Product product, VendorProductModel model)
        {
            var oldProductCategories = _categoryService.GetProductCategoriesByProductId(product.Id);
            var oldSelectedCategoryIds = oldProductCategories.Select(x => x.CategoryId).ToArray();

            var selectedCategoryIds = new List<int>();
            if (model.SelectedCategoryId > 0)
                selectedCategoryIds.Add(model.SelectedCategoryId);
            IEnumerable<int> needDeleteCategoryIds = oldSelectedCategoryIds.Except(selectedCategoryIds);
            IEnumerable<int> needAddCategoryIds = selectedCategoryIds.Except(oldSelectedCategoryIds.ToList());
            //Insert category mapping
            foreach (var categoryId in needAddCategoryIds)
            {
                InsertProductCategory(product.Id, categoryId);
            }
            //delete category mapping
            foreach (var categoryId in needDeleteCategoryIds)
            {
                var productCategory = oldProductCategories.FindProductCategory(product.Id, categoryId);
                DeleteProductCategory(productCategory);
            }
        }
        [NonAction]
        protected virtual void UpdateLocales(Product product, VendorProductModel model)
        {
            foreach (var localized in model.ProductModel.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(product,
                                                               x => x.Name,
                                                               localized.Name,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                                                               x => x.ShortDescription,
                                                               localized.ShortDescription,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                                                               x => x.FullDescription,
                                                               localized.FullDescription,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                                                               x => x.MetaKeywords,
                                                               localized.MetaKeywords,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                                                               x => x.MetaDescription,
                                                               localized.MetaDescription,
                                                               localized.LanguageId);
                _localizedEntityService.SaveLocalizedValue(product,
                                                               x => x.MetaTitle,
                                                               localized.MetaTitle,
                                                               localized.LanguageId);

                //search engine name
                var seName = product.ValidateSeName(localized.SeName, localized.Name, false).ToUrlString(true);
                _urlRecordService.SaveSlug(product, seName, localized.LanguageId);
            }
        }
        [NonAction]
        protected virtual void InsertProductCategory(int productId, int categoryId)
        {

            var existingProductCategories = _categoryService.GetProductCategoriesByCategoryId(categoryId, 0, int.MaxValue, true);
            if (existingProductCategories.FindProductCategory(productId, categoryId) == null)
            {
                var productCategory = new ProductCategory
                {
                    ProductId = productId,
                    CategoryId = categoryId,
                    DisplayOrder = 1000,
                };

                _categoryService.InsertProductCategory(productCategory);
            }


        }
        [NonAction]
        protected virtual void SaveProductSizeAttributeValues(Product product, VendorProductModel model, int productAttributeMappingId)
        {
            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeMappingId);
            if (productAttributeMapping == null)
            {
                productAttributeMappingId = CreateProductSizeAttributeMapping(product, model);
                productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeMappingId);
            }         

            if (productAttributeMapping != null && model.ProductSize != null)
            {
                foreach (var option in model.ProductSize)
                {
                    var productQuentity = option.Quantity;
                    var productAttributeValueModel = new ProductModel.ProductAttributeValueModel();
                    productAttributeValueModel.ProductAttributeMappingId = productAttributeMapping.Id;
                    var pav = new ProductAttributeValue
                    {
                        ProductAttributeMappingId = productAttributeValueModel.ProductAttributeMappingId,
                        Name = option.Name,
                        PriceAdjustment = option.PriceAdjustment,
                        Quantity = option.Quantity,

                    };

                    _productAttributeService.InsertProductAttributeValue(pav);

                }
            }

        }
         [NonAction]
        protected virtual void SaveProductColorAttributeValues(Product product, VendorProductModel model, int productAttributeMappingId)
        {
            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeMappingId);
            if (productAttributeMapping == null)
            {
                productAttributeMappingId = CreateProductColorAttributeMapping(product, model);
                productAttributeMapping = _productAttributeService.GetProductAttributeMappingById(productAttributeMappingId);
            }
            if (productAttributeMapping != null && model.ProductColor != null)
            {
                foreach (var option in model.ProductColor)
                {
                    var productQuentity = option.Quantity;
                    var productAttributeValueModel = new ProductModel.ProductAttributeValueModel();
                    productAttributeValueModel.ProductAttributeMappingId = productAttributeMapping.Id;
                    var pav = new ProductAttributeValue
                    {
                        ProductAttributeMappingId = productAttributeValueModel.ProductAttributeMappingId,
                        Name = option.Name,
                        PriceAdjustment = option.PriceAdjustment,
                        Quantity = option.Quantity,

                    };


                    _productAttributeService.InsertProductAttributeValue(pav);

                }

            }

        

        }
        [NonAction]
        protected virtual void PrepareAclModel(VendorProductModel model, Product product, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            model.ProductModel.AvailableCustomerRoles = _customerService
                .GetAllCustomerRoles(true)
                .Select(cr => cr.ToModel())
                .ToList();
            if (!excludeProperties)
            {
                if (product != null)
                {
                    model.ProductModel.SelectedCustomerRoleIds = _aclService.GetCustomerRoleIdsWithAccess(product);
                }
            }
        }
        [NonAction]
        private void SetPredefinedValues(ProductModel model)
        {
            model.MaximumCustomerEnteredPrice = 1000;
            model.MaxNumberOfDownloads = 10;
            model.RecurringCycleLength = 100;
            model.RecurringTotalCycles = 10;
            model.RentalPriceLength = 1;
            model.StockQuantity = 10000;
            model.NotifyAdminForQuantityBelow = 1;
            model.OrderMinimumQuantity = 1;
            model.OrderMaximumQuantity = 10000;
            model.ProductTypeId = (int)ProductType.SimpleProduct;
            model.ProductTemplateId = 1;
            model.ManageInventoryMethodId = 1;


            model.IsShipEnabled = true;
            model.Published = true;
            model.VisibleIndividually = true;
            model.UnlimitedDownloads = true;
            model.AllowCustomerReviews = false;
            model.SubjectToAcl = false;
            model.LimitedToStores = false;
            model.IsGiftCard = false;
            model.GiftCardTypeId = 0;
            model.RequireOtherProducts = false;
            model.AutomaticallyAddRequiredProducts = false;
            model.IsDownload = false;
            model.DownloadId = 0;
            model.UnlimitedDownloads = false;
            model.MaxNumberOfDownloads = 0;
            model.DownloadActivationTypeId = 0;
            model.HasSampleDownload = false;
            model.SampleDownloadId = 0;
            model.HasUserAgreement = false;
            model.IsRecurring = false;
            model.RecurringCycleLength = 0;
            model.RecurringCyclePeriodId = 0;
            model.RecurringTotalCycles = 0;
            model.IsRental = false;
            model.RentalPriceLength = 0;
            model.RentalPricePeriodId = 0;
            model.IsFreeShipping = true;
            model.ShipSeparately = false;
            model.AdditionalShippingCharge = 0;
            model.DeliveryDateId = 0;
            model.TaxCategoryId = 0;
            model.IsTelecommunicationsOrBroadcastingOrElectronicServices = false;
            model.UseMultipleWarehouses = false;
            model.AllowBackInStockSubscriptions = false;
            model.LowStockActivityId = 0;
            model.NotifyAdminForQuantityBelow = 1;
            model.BackorderModeId = 0;
            model.DisplayStockAvailability = false;
            model.MinStockQuantity = 0;
            model.DisplayStockQuantity = false;
            model.AllowedQuantities = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
        }
        [NonAction]
        protected virtual int CreateProductSizeAttributeMapping(Product product, VendorProductModel model)
         {
             var productAttributeMapping = new ProductAttributeMapping
             {
                 ProductId = product.Id,
                 ProductAttributeId = _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Size")).Count() > 0
                                         ? _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Size")).FirstOrDefault().Id
                                          : _productAttributeService.GetAllProductAttributes().FirstOrDefault().Id,
                 TextPrompt = "Size",
                 IsRequired = false,
                 AttributeControlTypeId = (Int32)AttributeControlType.DropdownList,
                 DisplayOrder = 0
             };
             _productAttributeService.InsertProductAttributeMapping(productAttributeMapping);
             return productAttributeMapping.Id;
         }
         [NonAction]
        protected virtual int CreateProductColorAttributeMapping(Product product, VendorProductModel model)
        {
            var productAttributeMapping = new ProductAttributeMapping
            {
                ProductId = product.Id,
                ProductAttributeId = _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Color")).Count() > 0
                                        ? _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Color")).FirstOrDefault().Id
                                         : _productAttributeService.GetAllProductAttributes().FirstOrDefault().Id,
                TextPrompt = "Color",
                IsRequired = false,
                AttributeControlTypeId = (Int32)AttributeControlType.DropdownList,
                DisplayOrder = 0
            };
            _productAttributeService.InsertProductAttributeMapping(productAttributeMapping);
            return productAttributeMapping.Id;
        }
        [NonAction]
        protected virtual void DeleteProductCategory(ProductCategory productCategory)
        {


            if (productCategory != null)
            {
                _categoryService.DeleteProductCategory(productCategory);
            }



        }
        [NonAction]
        protected virtual void DeleteProductPicture(ProductPicture productPicture)
        {
            if (productPicture != null)
            {
                _productService.DeleteProductPicture(productPicture);
            }

        }
        [NonAction]
        protected virtual int[] PrepareProductPictureIds(VendorProductModel model)
        {
            var result = new List<int>();
            if (model.PictureId1 > 0)
            {
                result.Add(model.PictureId1);

            }
            if (model.PictureId2 > 0)
            {
                result.Add(model.PictureId2);

            } if (model.PictureId3 > 0)
            {
                result.Add(model.PictureId3);

            } if (model.PictureId4 > 0)
            {
                result.Add(model.PictureId4);

            }
            return result.ToArray();
        }
        public ActionResult EditSizeAttributeValuesByProductId(int productId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(productId).Where(x => x.ProductAttribute.Name.Equals("Size")).FirstOrDefault();
           // var productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(productId).FirstOrDefault();
            if (productAttributeMapping == null)
            {
                #region product attribute
                // save product Attribute

                var productAttributeMappingEntity = new ProductAttributeMapping
                {
                 ProductId = product.Id,
                 ProductAttributeId = _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Size")).Count() > 0
                                         ? _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Size")).FirstOrDefault().Id
                                          : _productAttributeService.GetAllProductAttributes().FirstOrDefault().Id,
                 TextPrompt = "Size",
                 IsRequired = false,
                 AttributeControlTypeId = (Int32)AttributeControlType.DropdownList,
                 DisplayOrder = 0

                };
                _productAttributeService.InsertProductAttributeMapping(productAttributeMappingEntity);
                productAttributeMapping = productAttributeMappingEntity;

                #endregion
            }
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List", "Product");

            var model = new ProductModel.ProductAttributeValueListModel
            {
                ProductName = product.Name,
                ProductId = productAttributeMapping.ProductId,
                ProductAttributeName = productAttributeMapping.ProductAttribute.Name,
                ProductAttributeMappingId = productAttributeMapping.Id,
            };

            return PartialView("~/Plugins/Bs.VendorSystem/Views/BsVendor/EditAttributeValues.cshtml", model);
        }
        public ActionResult EditColorAttributeValuesByProductId(int productId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");
           // var productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(productId).FirstOrDefault();
            var productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(productId).Where(x => x.ProductAttribute.Name.Equals("Color")).FirstOrDefault();
            if (productAttributeMapping == null)
            {
                #region product attribute
                // save product Attribute

                var productAttributeMappingEntity = new ProductAttributeMapping
                {
                    ProductId = product.Id,
                    ProductAttributeId = _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Color")).Count() > 0
                                            ? _productAttributeService.GetAllProductAttributes().Where(x => x.Name.Equals("Color")).FirstOrDefault().Id
                                             : _productAttributeService.GetAllProductAttributes().FirstOrDefault().Id,
                    TextPrompt = "Color",
                    IsRequired = false,
                    AttributeControlTypeId = (Int32)AttributeControlType.DropdownList,
                    DisplayOrder = 0

                };
                _productAttributeService.InsertProductAttributeMapping(productAttributeMappingEntity);
                productAttributeMapping = productAttributeMappingEntity;

                #endregion
            }
            if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                return RedirectToAction("List", "Product");
            //var p = new VendorProductModel
             var specificationAttributes = _specificationAttributeService.GetSpecificationAttributes();
            var model = new VendorProductModel
            {
            };
               model.ProductModel.AddSpecificationAttributeModel.AvailableOptions.Add(new SelectListItem
                {
                    Text = "Select One",
                    Value = "0"
                });
                for (int i = 0; i < specificationAttributes.Count; i++)
                {
                    var sa = specificationAttributes[i];
                    model.ProductModel.AddSpecificationAttributeModel.AvailableAttributes.Add(new SelectListItem { Text = sa.Name, Value = sa.Id.ToString() });
                    if (i == 0)
                    {
                        //attribute options
                        foreach (var sao in _specificationAttributeService.GetSpecificationAttributeOptionsBySpecificationAttribute(sa.Id))
                            model.ProductModel.AddSpecificationAttributeModel.AvailableOptions.Add(new SelectListItem { Text = sao.Name, Value = sao.Id.ToString() });
                    }
                }
           

            model.ProductAttributeValueListModel = new ProductModel.ProductAttributeValueListModel
            {
                ProductName = product.Name,
                ProductId = productAttributeMapping.ProductId,
                ProductAttributeName = productAttributeMapping.ProductAttribute.Name,
                ProductAttributeMappingId = productAttributeMapping.Id,
            };

            return PartialView("~/Plugins/Bs.VendorSystem/Views/BsVendor/EditColorAttributeValues.cshtml", model);
        }
        protected ActionResult AccessDeniedView()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/AccessDenied.cshtml");
        }
        public ActionResult GetAllSubCategoriesByParentCategoryId (int parentCategoryId,
            bool showHidden = false, bool includeAllLevels = false)
        {
            var firstSubCategory = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, showHidden: true);
            var result = (from s in firstSubCategory
                          select new { id = s.Id, name = s.Name }).ToList();
            result.Insert(0, new { id = 0, name ="Select One"});
           
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Order

        #region OrderList
        public ActionResult OrderList(int? orderStatusId = null,
int? paymentStatusId = null, int? shippingStatusId = null)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //order statuses
            var model = new OrderListModel();
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (orderStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableOrderStatuses.FirstOrDefault(x => x.Value == orderStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (paymentStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailablePaymentStatuses.FirstOrDefault(x => x.Value == paymentStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //shipping statuses
            model.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.AvailableShippingStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (shippingStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableShippingStatuses.FirstOrDefault(x => x.Value == shippingStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //warehouses
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var w in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = w.Name, Value = w.Id.ToString() });

            //a vendor should have access only to orders with his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            // return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/OrderList.cshtml", model);
        }
        [HttpPost]
        public ActionResult OrderListKendo(DataSourceRequest command, OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.VendorId = _workContext.CurrentVendor.Id;
            }

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            //var filterByProductId = 0;
            var product = _productService.GetProductById(model.ProductId);
            // if (product != null && HasAccessToProduct(product))
            //  filterByProductId = model.ProductId;

            //load orders
            
            var orders = _orderService.SearchOrders(storeId: model.StoreId,
                vendorId: model.VendorId,
                productId: model.ProductId,
                warehouseId: model.WarehouseId,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                billingEmail: model.BillingEmail,
                orderGuid: model.OrderGuid,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = orders.Select(x =>
                {
                    var store = _storeService.GetStoreById(x.StoreId);
                    return new OrderModel
                    {
                        Id = x.Id,
                        StoreName = store != null ? store.Name : "Unknown",
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderStatus = x.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        PaymentStatus = x.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                        ShippingStatus = x.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                        CustomerEmail = x.BillingAddress.Email,
                        CustomerFullName = string.Format("{0} {1}", x.BillingAddress.FirstName, x.BillingAddress.LastName),
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                    };
                }),
                Total = orders.TotalCount
            };


            var reportSummary = _orderReportService.GetOrderAverageReportLine(
                storeId: model.StoreId,
                vendorId: model.VendorId,
                // orderId: 0,
                //paymentMethodSystemName: model.PaymentMethodSystemName,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                startTimeUtc: startDateValue,
                endTimeUtc: endDateValue,
                billingEmail: model.BillingEmail
                // billingLastName: model.BillingLastName,
                //billingCountryId: model.BillingCountryId,
                // orderNotes: model.OrderNotes
                );
            var profit = _orderReportService.ProfitReport(
                storeId: model.StoreId,
                vendorId: model.VendorId,
                // paymentMethodSystemName: model.PaymentMethodSystemName,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                startTimeUtc: startDateValue,
                endTimeUtc: endDateValue,
                billingEmail: model.BillingEmail
                // billingLastName: model.BillingLastName,
                // billingCountryId: model.BillingCountryId,
                // orderNotes: model.OrderNotes
               );
            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if (primaryStoreCurrency == null)
                throw new Exception("Cannot load primary store currency");

            gridModel.ExtraData = new OrderAggreratorModel
            {
                aggregatorprofit = _priceFormatter.FormatPrice(profit, true, false),
                aggregatorshipping = _priceFormatter.FormatShippingPrice(reportSummary.SumShippingExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false),
                aggregatortax = _priceFormatter.FormatPrice(reportSummary.SumTax, true, false),
                aggregatortotal = _priceFormatter.FormatPrice(reportSummary.SumOrders, true, false)
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
        #endregion

        #region OrderEdit
        public ActionResult OrderEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null || order.Deleted)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return RedirectToAction("List");

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/OrderEdit.cshtml", model);
        }
        #endregion

        #region OrderNote
        [HttpPost]
        public ActionResult OrderNotesSelect(int orderId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return Content("");

            //order notes
            var orderNoteModels = new List<OrderModel.OrderNote>();
            foreach (var orderNote in order.OrderNotes
                .OrderByDescending(on => on.CreatedOnUtc))
            {
                var download = _downloadService.GetDownloadById(orderNote.DownloadId);
                orderNoteModels.Add(new OrderModel.OrderNote
                {
                    Id = orderNote.Id,
                    OrderId = orderNote.OrderId,
                    DownloadId = orderNote.DownloadId,
                    DownloadGuid = download != null ? download.DownloadGuid : Guid.Empty,
                    DisplayToCustomer = orderNote.DisplayToCustomer,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }

            var gridModel = new DataSourceResult
            {
                Data = orderNoteModels,
                Total = orderNoteModels.Count
            };

            return Json(gridModel);
        }
        [ValidateInput(false)]
        public ActionResult OrderNoteAdd(int orderId, int downloadId, bool displayToCustomer, string message)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            var orderNote = new OrderNote
            {
                DisplayToCustomer = displayToCustomer,
                Note = message,
                DownloadId = downloadId,
                CreatedOnUtc = DateTime.UtcNow,
            };
            order.OrderNotes.Add(orderNote);
            _orderService.UpdateOrder(order);

            //new order notification
            if (displayToCustomer)
            {
                //email
                _workflowMessageService.SendNewOrderNoteAddedCustomerNotification(
                    orderNote, _workContext.WorkingLanguage.Id);

            }

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult OrderNoteDelete(int id, int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var orderNote = order.OrderNotes.FirstOrDefault(on => on.Id == id);
            if (orderNote == null)
                throw new ArgumentException("No order note found with the specified id");
            _orderService.DeleteOrderNote(orderNote);

            return new NullJsonResult();
        }
        #endregion

        #endregion

        #region Shipment
        public ActionResult ShipmentList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new ShipmentListModel();
            //countries
            model.AvailableCountries.Add(new SelectListItem { Text = "*", Value = "0" });
            foreach (var c in _countryService.GetAllCountries(showHidden: true))
                model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            //states
            model.AvailableStates.Add(new SelectListItem { Text = "*", Value = "0" });

            //warehouses
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var w in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = w.Name, Value = w.Id.ToString() });

            //return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/ShipmentList.cshtml", model);
        }
        [HttpPost]
        public ActionResult ShipmentListSelect(DataSourceRequest command, ShipmentListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            //load shipments
            var shipments = _shipmentService.GetAllShipments(vendorId: vendorId,
                warehouseId: model.WarehouseId, shippingCountryId: model.CountryId,
                shippingStateId: model.StateProvinceId, shippingCity: model.City,
                trackingNumber: model.TrackingNumber,
                loadNotShipped: model.LoadNotShipped,
                createdFromUtc: startDateValue, createdToUtc: endDateValue,
                pageIndex: command.Page - 1, pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = shipments.Select(shipment => PrepareShipmentModel(shipment, false)),
                Total = shipments.TotalCount
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
        [HttpPost]
        public ActionResult ShipmentsItemsByShipmentId(int shipmentId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                throw new ArgumentException("No shipment found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return Content("");

            var order = _orderService.GetOrderById(shipment.OrderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return Content("");

            //shipments
            var shipmentModel = PrepareShipmentModel(shipment, true);
            var gridModel = new DataSourceResult
            {
                Data = shipmentModel.Items,
                Total = shipmentModel.Items.Count
            };

            return Json(gridModel);
        }
        public ActionResult SetAsShippedSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                shipments.AddRange(_shipmentService.GetShipmentsByIds(selectedIds.ToArray()));
            }
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                shipments = shipments.Where(HasAccessToShipment).ToList();
            }

            foreach (var shipment in shipments)
            {
                try
                {
                    _orderProcessingService.Ship(shipment, true);
                }
                catch
                {
                    //ignore any exception
                }
            }

            return Json(new { Result = true });
        }
        public ActionResult SetAsDeliveredSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                shipments.AddRange(_shipmentService.GetShipmentsByIds(selectedIds.ToArray()));
            }
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                shipments = shipments.Where(HasAccessToShipment).ToList();
            }

            foreach (var shipment in shipments)
            {
                try
                {
                    _orderProcessingService.Deliver(shipment, true);
                }
                catch
                {
                    //ignore any exception
                }
            }

            return Json(new { Result = true });
        }
        [HttpPost]
        public ActionResult ShipmentsByOrder(int orderId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return Content("");

            //shipments
            var shipmentModels = new List<ShipmentModel>();
            var shipments = order.Shipments
                //a vendor should have access only to his products
                .Where(s => _workContext.CurrentVendor == null || HasAccessToShipment(s))
                .OrderBy(s => s.CreatedOnUtc)
                .ToList();
            foreach (var shipment in shipments)
                shipmentModels.Add(PrepareShipmentModel(shipment, false));

            var gridModel = new DataSourceResult
            {
                Data = shipmentModels,
                Total = shipmentModels.Count
            };


            return Json(gridModel);
        }

        #region Packege Slip
        public ActionResult PdfPackagingSlip(int shipmentId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();
            var model = new ShipmentListModel();
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                //no shipment found with the specified id
              //  return RedirectToAction("List");
                return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/ShipmentList.cshtml", model);
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
              //  return RedirectToAction("List");
                return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/ShipmentList.cshtml", model);
            var shipments = new List<Shipment>();
            shipments.Add(shipment);

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintPackagingSlipsToPdf(stream,shipments,_workContext.WorkingLanguage.Id);
               // _pdfService.PrintPackagingSlipsToPdf(stream, shipments, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("packagingslip_{0}.pdf", shipment.Id));
        }
        public ActionResult PdfPackagingSlipAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();
            var model = new ShipmentListModel();
            //a vendor should have access only to his products
            DateTime? startDateValue = (model.StartDate == null) ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

           // var shipments = _shipmentService.GetAllShipments(vendorId: vendorId).ToList();
            var shipments = _shipmentService.GetAllShipments(vendorId: vendorId,
    warehouseId: model.WarehouseId,
    shippingCountryId: model.CountryId,
    shippingStateId: model.StateProvinceId,
    shippingCity: model.City,
    trackingNumber: model.TrackingNumber,
    loadNotShipped: model.LoadNotShipped,
    createdFromUtc: startDateValue,
    createdToUtc: endDateValue);


            //ensure that we at least one shipment selected
            if (shipments.Count == 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.Shipments.NoShipmentsSelected"));
                //return RedirectToAction("ShipmentList");
                return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/ShipmentList.cshtml", model);
            }
          
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintPackagingSlipsToPdf(stream, shipments, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "packagingslips.pdf");
        }
        public ActionResult PdfPackagingSlipSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();
            var model = new ShipmentListModel();
            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                shipments.AddRange(_shipmentService.GetShipmentsByIds(ids));
            }
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                shipments = shipments.Where(HasAccessToShipment).ToList();
            }

            //ensure that we at least one shipment selected
            if (shipments.Count == 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.Shipments.NoShipmentsSelected"));
                //return RedirectToAction("ShipmentList");
                return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/ShipmentList.cshtml", model);
            }

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintPackagingSlipsToPdf(stream, shipments, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "packagingslips.pdf");
        }
        #endregion
        #endregion

        #region ProductLike
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/BsLike/Configure.cshtml");
        }
        #region Utility
        public void PrepareLikeModel(IPagedList<LikeInfoTable> data, BsLikeListModelForCustomer model)
        {
            foreach (var singleData in data)
            {
                var product = _productService.GetProductById(singleData.ProductId);
                if (product == null || product.Deleted)
                    continue;
                var singleModel = new BsLikeModelForCustomer();
                singleModel.Id = singleData.Id;
                singleModel.ProductName = product.GetLocalized(x => x.Name);
                singleModel.ProductSeName = product.GetSeName();
                int pictureSize = 80;
                //prepare picture model
                var defaultProductPictureCacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_DEFAULTPICTURE_MODEL_KEY, product.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                singleModel.Picture = _cacheManager.Get(defaultProductPictureCacheKey, () =>
                {
                    var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                    var pictureModel = new PictureModel
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture)
                    };
                    //"title" attribute
                    pictureModel.Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute)) ?
                        picture.TitleAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), singleModel.ProductName);
                    //"alt" attribute
                    pictureModel.AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute)) ?
                        picture.AltAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), singleModel.ProductName);

                    return pictureModel;
                });

                model.LikeList.Add(singleModel);
            }
        }
        #endregion
        #region adminLike
        [AdminAuthorize]
        public ActionResult AdminLike()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/BsLike/AdminLike.cshtml");
        }
        [HttpPost]
        public ActionResult AdminLikeList(DataSourceRequest command)
        {
            var model = _bsVendorService.MostLikedProduct(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = model.Select(x => new
                {
                    PictureThumbnailUrl = _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(x.ProductId, 1).FirstOrDefault(), 80),
                    Name = _productService.GetProductById(x.ProductId).GetSeName(),
                    Count = x.Count
                }),
                Total = model.TotalCount
            };

            return Json(gridModel);
        }
        #endregion
        #region Product
        public ActionResult AddDataToCurrentViewer(int producId)
        {
            if (_bsVendorService.IsAvailable(producId, _workContext.CurrentCustomer.Id))
            {
                _bsVendorService.UpdateLike(producId, _workContext.CurrentCustomer.Id);
            }
            else
            {
                var data = new LikeInfoTable
                {
                    ProductId = producId,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    IsRegisterdCustomer = _workContext.CurrentCustomer.IsRegistered()
                };
                _bsVendorService.InsertLike(data);

            }
            var model = new
            {
                Success = true
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCurrentView(int producId)
        {
            int num = _bsVendorService.GetCount(producId);
            var model = new
            {
                Number = num
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [ChildActionOnly]
        public ActionResult ProductboxInfo(string widgetZone, object additionalData = null)
        {
            int productId = (int)additionalData;
            var model = new BsLikeProductBoxModel
            {
                ProductId = productId,
                LikeCout = _bsVendorService.GetCount(productId),
                IsLikedByCurrentCustomer = _bsVendorService.IsLikedByCustomer(productId, _workContext.CurrentCustomer.Id),
                IsGuestCustomer = _workContext.CurrentCustomer.IsGuest()
            };

            return PartialView("~/Plugins/Bs.VendorSystem/Views/BsLike/ProductBoxView.cshtml", model);
        }
        [ChildActionOnly]
        public ActionResult LikeHeader(string widgetZone, object additionalData = null)
        {

            var model = new BsLikeHeaderModel
            {
                IsGuest = _workContext.CurrentCustomer.IsGuest(),
                Count = _bsVendorService.GetLikeCountOfCustomer(_workContext.CurrentCustomer.Id)
            };

            return PartialView("~/Plugins/Bs.VendorSystem/Views/BsLike/LikeHeader.cshtml", model);
        }
        public ActionResult ProductLike(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new Exception("No product found with this id."); ;

            if (!_workContext.CurrentCustomer.IsRegistered())
                throw new Exception("Anonymous Follow is not allowed! Please Login and then try again.");
            string responseError = string.Empty;


            if (_bsVendorService.IsLikedByCustomer(product.Id, _workContext.CurrentCustomer.Id))
                throw new Exception("You are already liked the product. Please the refresh page.");
            try
            {
                _bsVendorService.UpdateLike(product.Id, _workContext.CurrentCustomer.Id);
                int productLikeCount = _bsVendorService.GetCount(product.Id);
                int likeCountOfPerson = _bsVendorService.GetLikeCountOfCustomer(_workContext.CurrentCustomer.Id);
                return Json(new { success = true, liked_unliked = 1, productlikecounthtml = productLikeCount.ToString(), message = "Your like  request Success! ", likeproperty = true, likecounthtml = "(" + likeCountOfPerson + ")" });

            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
                responseError += exc.Message;
            }

            return Json(new { success = false, message = responseError });
        }
        public ActionResult ProductUnLike(int productId)
        {

            if (!_workContext.CurrentCustomer.IsRegistered())
                throw new Exception("Anonymous Follow is not allowed! Please Login and then try again.");
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new Exception("No product found with this id.");

            if (!_bsVendorService.IsLikedByCustomer(product.Id, _workContext.CurrentCustomer.Id))
                throw new Exception("You are already unliked  the product. Please refresh the page.");
            string responseError = string.Empty;

            try
            {

                _bsVendorService.UpdateLike(product.Id, _workContext.CurrentCustomer.Id, true);
                int productLikeCount = _bsVendorService.GetCount(product.Id);
                int likeCountOfPerson = _bsVendorService.GetLikeCountOfCustomer(_workContext.CurrentCustomer.Id);
                return Json(new { success = true, liked_unliked = 1, productlikecounthtml = productLikeCount.ToString(), message = "Your unlike  request Success! ", likeproperty = false, likecounthtml = "(" + likeCountOfPerson + ")" });
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
                responseError += exc.Message;
            }

            return Json(new { success = false, message = responseError });
        }
        public ActionResult LikeListForCustomer(int page = 1)
        {
            if (page >= 1)
                page = page - 1;
            if (_workContext.CurrentCustomer.IsGuest())
                return RedirectToRoute("HomePage");
            var list = _bsVendorService.GetLikedProductByCustomer(_workContext.CurrentCustomer.Id, page, 10);
            var model = new BsLikeListModelForCustomer();
            PrepareLikeModel(list, model);
            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.LikelistForCustomer",
                UseRouteLinks = true,
                RouteValues = new BsLikeRouteValues { page = page }
            };
            return View("~/Plugins/Bs.VendorSystem/Views/BsLike/LikeListForCustomer.cshtml", model);

        }
        public ActionResult LikeListForVendor(int vendorId)
        {
            int page = 0;
            var vendor = _vendorService.GetVendorById(vendorId);
            var customer = _bsVendorService.GetCustomerByVendorEmail(vendor.Email);

            var list = _bsVendorService.GetLikedProductByCustomer(customer.Id, page, 10);
            var model = new BsLikeListModelForCustomer();
            PrepareLikeModel(list, model);
            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.LikelistForCustomer",
                UseRouteLinks = true,
                RouteValues = new BsLikeRouteValues { page = page }
            };
            return View("~/Plugins/Bs.VendorSystem/Views/BsLike/LikeListForVendor.cshtml", model);

        }
        [HttpPost, ActionName("LikeListForCustomer")]
        [FormValueRequired("removefromcart")]
        public ActionResult DeleteFromLikelist(FormCollection form, int page = 1)
        {
            if (page >= 1)
                page = page - 1;

            if (_workContext.CurrentCustomer.IsGuest())
                return RedirectToRoute("HomePage");


            var allIdsToRemove = form["removefromcart"] != null
                ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToList()
                : new List<int>();


            foreach (var id in allIdsToRemove)
            {
                _bsVendorService.DeleteLike(id);
            }

            var list = _bsVendorService.GetLikedProductByCustomer(_workContext.CurrentCustomer.Id, page, 10);
            var model = new BsLikeListModelForCustomer();
            PrepareLikeModel(list, model);
            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.LikelistForCustomer",
                UseRouteLinks = true,
                RouteValues = new BsLikeRouteValues { page = page }
            };
            if (model.LikeList.Count == 0)
                return RedirectToRoute("Bs.VendorSystem.LikelistForCustomer");

            return View("~/Plugins/Bs.VendorSystem/Views/BsLike/LikeListForCustomer.cshtml", model);
        }
        #endregion
        #endregion

        #region follow
        #region Utility
        public void PrepareFollowModel(IPagedList<BsVendorFollowInfoTable> data, BsVendorFollowListModelForCustomer model)
        {
            foreach (var singleData in data)
            {
                // var product = _productService.GetProductById(singleData.VendorId);
                var vendor = _vendorService.GetVendorById(singleData.VendorId);
                if (vendor == null || vendor.Deleted)
                    continue;
                //var singleModel = new BsLikeModelForCustomer();
                var singleModel = new BsVendorFollowModelForCustomer();
                singleModel.Id = singleData.Id;
                singleModel.Name = vendor.Name;
                singleModel.Email = vendor.Email;
                singleModel.CustomerName = _customerService.GetCustomerById(singleData.CustomerId).GetFullName();
                singleModel.VendorSeName = vendor.GetSeName();
                model.FollowList.Add(singleModel);

                //model.LikeList.Add(singleModel);
            }
        }
        #endregion
        #region admin Follow
        [AdminAuthorize]
        public ActionResult AdminFollow()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/BsFollows/AdminFollow.cshtml");
        }
        [HttpPost]
        public ActionResult AdminFollowList(DataSourceRequest command)
        {
            //var model = _bsVendorService.MostLikedProduct(command.Page - 1, command.PageSize);
            var model = _bsVendorService.MostFollowsVendor(command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = model.Select(x => new
                {
                    // PictureThumbnailUrl = _pictureService.GetPictureUrl(_pictureService.GetPictureById())
                    //PictureThumbnailUrl = _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(x.ProductId, 1).FirstOrDefault(), 80),
                    //Name = _productService.GetProductById(x.ProductId).GetSeName(),
                    //Count = x.Count
                    Name = _vendorService.GetVendorById(x.VendorId).GetSeName(),
                    Email = _vendorService.GetVendorById(x.VendorId).Email,
                    Count = x.Count,


                }),
                Total = model.TotalCount
            };

            return Json(gridModel);
        }
        #endregion
        #region CurrentCustomer/Vendor
        public ActionResult AddVendorDataToCurrentViewer(int vendorId)
        {
            if (_bsVendorService.IsAvailableVendor(vendorId, _workContext.CurrentCustomer.Id))
            {
                _bsVendorService.UpdateFollow(vendorId, _workContext.CurrentCustomer.Id);
            }
            else
            {
                var data = new BsVendorFollowInfoTable
                {
                    VendorId = vendorId,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    IsRegisterdCustomer = _workContext.CurrentCustomer.IsRegistered()
                };
                _bsVendorService.InsertFollow(data);
            }


            var model = new
            {
                Success = true
            };
            return Json(model, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetFollowCurrentView(int vendorId)
        {
            //int num = _bsVendorService.GetCount(producId);
            int num = _bsVendorService.GetFollowCount(vendorId);
            var model = new
            {
                Number = num
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [ChildActionOnly]
        public ActionResult FollowProductboxInfo(string widgetZone, object additionalData = null)
        {
            var routeData = ((System.Web.UI.Page)this.HttpContext.CurrentHandler).RouteData;
            int vendorId = (int)routeData.Values["vendorId"];
            //int productid = (int)additionalData;
            //var product = _productService.GetProductById(productid);

            //var vendorId = product.VendorId;
            if (vendorId == null)
                throw new Exception("No vendor found with this id.");


            var model = new BsFollowProductBoxModel
            {
                VendorId = vendorId,
                FollowCout = _bsVendorService.GetFollowCount(vendorId),
                IsFollowedByCurrentCustomer = _bsVendorService.IsFollowByCustomer(vendorId, _workContext.CurrentCustomer.Id),
                IsGuestCustomer = _workContext.CurrentCustomer.IsGuest()
            };

            return PartialView("~/Plugins/Bs.VendorSystem/Views/BsFollows/FollowProductBoxView.cshtml", model);
        }
        public ActionResult FolowHeader(string widgetZone, object additionalData = null)
        {

            var model = new BsFollowHeaderModel
            {
                IsGuest = _workContext.CurrentCustomer.IsGuest(),
                Count = _bsVendorService.GetFollowCountOfCustomer(_workContext.CurrentCustomer.Id)
            };

            return PartialView("~/Plugins/Bs.VendorSystem/Views/BsFollows/FollowHeader.cshtml", model);
        }
        public ActionResult VendorFollow(int vendorId)
        {
            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null)
            {
                throw new Exception("No vendor found with this id.");
            }


            if (!_workContext.CurrentCustomer.IsRegistered())
                throw new Exception("Anonymous Follow is not allowed! Please Login and then try again.");
            string responseError = string.Empty;


            if (_bsVendorService.IsFollowByCustomer(vendor.Id, _workContext.CurrentCustomer.Id))
                throw new Exception("You are already followed the Vendor. Please the refresh page.");
            try
            {
                _bsVendorService.UpdateFollow(vendor.Id, _workContext.CurrentCustomer.Id);
                int vendorFollowedCount = _bsVendorService.GetFollowCount(vendor.Id);
                int followCountOfPerson = _bsVendorService.GetFollowCountOfCustomer(_workContext.CurrentCustomer.Id);
                return Json(new { success = true, follow_unFollowed = 1, vendorFollowedCountHtml = vendorFollowedCount.ToString(), message = "Your Follow  request Success! ", likeproperty = true, likecounthtml = "(" + followCountOfPerson + ")" });

            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
                responseError += exc.Message;
            }

            return Json(new { success = false, message = responseError });
        }
        public ActionResult VendorUnFollow(int vendorId)
        {

            if (!_workContext.CurrentCustomer.IsRegistered())
                throw new Exception("Anonymous Follow is not allowed! Please Login and then try again.");
            var vendor = _vendorService.GetVendorById(vendorId);

            if (vendor == null)
                throw new Exception("No Vendor found with this id.");
            if (!_bsVendorService.IsFollowByCustomer(vendor.Id, _workContext.CurrentCustomer.Id))
                throw new Exception("You are already unFollowed  the Vendor. Please refresh the page.");
            string responseError = string.Empty;

            try
            {
                _bsVendorService.UpdateFollow(vendor.Id, _workContext.CurrentCustomer.Id, true);
                int vendorFollowedCount = _bsVendorService.GetFollowCount(vendor.Id);
                int followCountOfPerson = _bsVendorService.GetFollowCountOfCustomer(_workContext.CurrentCustomer.Id);
                return Json(new { success = true, follow_unFollowed = 1, vendorFollowedCountHtml = vendorFollowedCount.ToString(), message = "Your Follow  request Success! ", likeproperty = true, likecounthtml = "(" + followCountOfPerson + ")" });
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
                responseError += exc.Message;
            }

            return Json(new { success = false, message = responseError });
        }
        public ActionResult FollowListForCustomer(int page = 1)
        {
            if (page >= 1)
                page = page - 1;
            if (_workContext.CurrentCustomer.IsGuest())
                return RedirectToRoute("HomePage");
            // var list = _bsVendorService.GetLikedProductByCustomer(_workContext.CurrentCustomer.Id, page, 10);
            var list = _bsVendorService.GetFollowVendorByCustomer(_workContext.CurrentCustomer.Id, page, 10);
            var model = new BsVendorFollowListModelForCustomer();
            //PrepareLikeModel(list, model);
            PrepareFollowModel(list, model);
            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.FollowListForCustomer",
                UseRouteLinks = true,
                RouteValues = new BsFollowRouteValues { page = page }
            };
            return View("~/Plugins/Bs.VendorSystem/Views/BsFollows/FollowListForCustomer.cshtml", model);

        }
        public ActionResult FollowerListForVendor(int vendorId)
        {
            int page = 1;
            if (page >= 1)
                page = page - 1;
            var vendor = _vendorService.GetVendorById(vendorId);
            var customer = _bsVendorService.GetCustomerByVendorEmail(vendor.Email);
            var list = _bsVendorService.GetFollowCustomerByVendorId(vendorId, page, 10);
            var model = new BsVendorFollowListModelForCustomer();
            //PrepareLikeModel(list, model);
            PrepareFollowModel(list, model);
            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.FollowListForCustomer",
                UseRouteLinks = true,
                RouteValues = new BsFollowRouteValues { page = page }
            };
            return View("~/Plugins/Bs.VendorSystem/Views/BsFollows/FollowListForVendor.cshtml", model);

        }
        [HttpPost, ActionName("FollowListForCustomer")]
        [FormValueRequired("removefromcart")]
        public ActionResult DeleteFromFollowlist(FormCollection form, int page = 1)
        {
            if (page >= 1)
                page = page - 1;

            if (_workContext.CurrentCustomer.IsGuest())
                return RedirectToRoute("HomePage");


            var allIdsToRemove = form["removefromcart"] != null
                ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToList()
                : new List<int>();


            foreach (var id in allIdsToRemove)
            {
                _bsVendorService.DeleteFollow(id);
            }

            //var list = _bsVendorService.GetLikedProductByCustomer(_workContext.CurrentCustomer.Id, page, 10);
            var list = _bsVendorService.GetFollowVendorByCustomer(_workContext.CurrentCustomer.Id, page, 10);
            //  var model = new BsLikeListModelForCustomer();
            var model = new BsVendorFollowListModelForCustomer();
            PrepareFollowModel(list, model);
            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.FollowListForCustomer",
                UseRouteLinks = true,
                RouteValues = new BsFollowRouteValues { page = page }
            };
            if (model.FollowList.Count == 0)
                return RedirectToRoute("Bs.VendorSystem.FollowListForCustomer");

            return View("~/Plugins/Bs.VendorSystem/Views/BsFollows/FollowListForCustomer.cshtml", model);
        }
        #endregion
        #endregion

        #region Notification

        public void PrepareNotificationModel(IPagedList<BsVendorNotificationInfoTable> data,
            BsVendorNotificationListModelForCustomer model)
        {
            foreach (var singleData in data)
            {
                var notifier = _customerService.GetCustomerById(singleData.NotifierCustomerId);
                var notifying = _customerService.GetCustomerById(singleData.NotifyingCustomerId);


                var notificationModel = new BsVendorNotificationModelForCustomer();
                notificationModel.Id = singleData.Id;
                notificationModel.NotifierCustomerId = singleData.NotifierCustomerId;
                notificationModel.NotifierCustomerName = notifier.GetFullName();
                notificationModel.NotifierCustomerEmail = notifier.Email;

                notificationModel.NotifyingCustomerId = singleData.NotifyingCustomerId;
                notificationModel.NotifyingCustomerName = notifying.GetFullName();
                notificationModel.NotifyingCustomerName = notifying.Email;
                notificationModel.ItemId = singleData.ItemTypeId;
                var vendor = _vendorService.GetVendorById(singleData.ItemId);
                if (vendor != null)
                {
                    notificationModel.VendorName = vendor.Name;
                }
                else
                {
                    notificationModel.VendorName = notifier.GetFullName();
                }


                if (singleData.ItemTypeId == 100 || singleData.ItemTypeId == 200 || singleData.ItemTypeId == 500 || singleData.ItemTypeId == 600)
                {
                    var product = _productService.GetProductById(singleData.ItemId);
                    notificationModel.ProductId = product.Id;
                    notificationModel.ProductName = product.GetLocalized(x => x.Name);
                    notificationModel.ProductSeName = product.GetSeName();
                    int pictureSize = 80;
                    var defaultProductPictureCacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_DEFAULTPICTURE_MODEL_KEY, product.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                    notificationModel.Picture = _cacheManager.Get(defaultProductPictureCacheKey, () =>
                    {
                        var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                        var pictureModel = new PictureModel
                        {
                            ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                            FullSizeImageUrl = _pictureService.GetPictureUrl(picture)
                        };
                        //"title" attribute
                        pictureModel.Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute)) ?
                            picture.TitleAttribute :
                            string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), notificationModel.ProductName);
                        //"alt" attribute
                        pictureModel.AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute)) ?
                            picture.AltAttribute :
                            string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), notificationModel.ProductName);

                        return pictureModel;
                    });


                }

                model.NotificatioList.Add(notificationModel);


            }
        }

        [AdminAuthorize]
        public ActionResult Notification()
        {
            return View("~/Plugins/Bs.VendorSystem/Views/Notification/Notification.cshtml");
        }

        public ActionResult UnViewNotificationList(int page = 1)
        {

            if (page >= 1)
                page = page - 1;
            if (_workContext.CurrentCustomer.IsGuest())
                return RedirectToRoute("HomePage");

            var customerId = _workContext.CurrentCustomer.Id;
            var notificationlist = _bsVendorService.GetUnViewNotificationByCustomerId(customerId, page, 10);
            var model = new BsVendorNotificationListModelForCustomer();
            PrepareNotificationModel(notificationlist, model);
            model.PagerModel = new PagerModel
            {
                PageSize = notificationlist.PageSize,
                TotalRecords = notificationlist.TotalCount,
                PageIndex = notificationlist.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.UnViewNotificationList",
                UseRouteLinks = true,
                RouteValues = new BsFollowRouteValues { page = page }
            };
            foreach (var notification in notificationlist)
            {

                var nt = _bsVendorService.GetNotificationById(notification.Id);
                if (nt != null)
                {
                    nt.IsViewed = true;
                    nt.NotificationUpDateTimeUtc = DateTime.UtcNow;
                    _bsVendorService.UpdateVendorNotification(nt);
                }


            }

            return View("~/Plugins/Bs.VendorSystem/Views/Notification/NotificationListForCustomer.cshtml", model);

        }

        public ActionResult AllNotificationList(int page = 1)
        {

            if (page >= 1)
                page = page - 1;
            if (_workContext.CurrentCustomer.IsGuest())
                return RedirectToRoute("HomePage");

            var customerId = _workContext.CurrentCustomer.Id;
            var notificationlist = _bsVendorService.GetAllNotificationByCustomerId(customerId, page, 10);
            var model = new BsVendorNotificationListModelForCustomer();
            PrepareNotificationModel(notificationlist, model);
            model.PagerModel = new PagerModel
            {
                PageSize = notificationlist.PageSize,
                TotalRecords = notificationlist.TotalCount,
                PageIndex = notificationlist.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "Bs.VendorSystem.UnViewNotificationList",
                UseRouteLinks = true,
                RouteValues = new BsFollowRouteValues { page = page }
            };
            //foreach (var notification in notificationlist)
            //{

            //    var nt = _bsVendorService.GetNotificationById(notification.Id);
            //    if (nt != null)
            //    {
            //        nt.IsViewed = true;
            //        nt.NotificationUpDateTimeUtc = DateTime.UtcNow;
            //        _bsVendorService.UpdateVendorNotification(nt);
            //    }


            //}

            return View("~/Plugins/Bs.VendorSystem/Views/Notification/NotificationListForCustomer.cshtml", model);

        }

        public ActionResult NotificationHeader(string widgetZone, object additionalData = null)
        {
            var customerId = _workContext.CurrentCustomer.Id;
            var totalNumberOfUnviewNotification = _bsVendorService.CountUnViewingNotificationByCustomerId(customerId);
            var model = new BsVendorNotificationCountModel();
            model.CustomerId = customerId;
            model.TotalNumberOfUnviewNotification = totalNumberOfUnviewNotification;
            //if (model.TotalNumberOfUnviewNotification == 0)
            //{
            //    return Content("");
            //}
            return PartialView("~/Plugins/Bs.VendorSystem/Views/Notification/NotificationHeader.cshtml", model);
        }


        #endregion

        #region VendorPage

        protected virtual ActionResult InvokeHttp404()
        {
            // Call target Controller and pass the routeData.
            IController errorController = EngineContext.Current.Resolve<CommonController>();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Common");
            routeData.Values.Add("action", "PageNotFound");

            errorController.Execute(new RequestContext(this.HttpContext, routeData));

            return new EmptyResult();
        }
        [NonAction]
        protected virtual void PrepareSortingOptions(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            if (command == null)
                throw new ArgumentNullException("command");

            pagingFilteringModel.AllowProductSorting = _catalogSettings.AllowProductSorting;
            if (pagingFilteringModel.AllowProductSorting)
            {
                foreach (ProductSortingEnum enumValue in Enum.GetValues(typeof(ProductSortingEnum)))
                {
                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "orderby=" + ((int)enumValue).ToString(), null);

                    var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                    pagingFilteringModel.AvailableSortOptions.Add(new SelectListItem
                    {
                        Text = sortValue,
                        Value = sortUrl,
                        Selected = enumValue == (ProductSortingEnum)command.OrderBy
                    });
                }
            }
        }

        [NonAction]
        protected virtual void PrepareViewModes(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            if (command == null)
                throw new ArgumentNullException("command");

            pagingFilteringModel.AllowProductViewModeChanging = _catalogSettings.AllowProductViewModeChanging;

            var viewMode = !string.IsNullOrEmpty(command.ViewMode)
                ? command.ViewMode
                : _catalogSettings.DefaultViewMode;
            pagingFilteringModel.ViewMode = viewMode;
            if (pagingFilteringModel.AllowProductViewModeChanging)
            {
                var currentPageUrl = _webHelper.GetThisPageUrl(true);
                //grid
                pagingFilteringModel.AvailableViewModes.Add(new SelectListItem
                {
                    Text = _localizationService.GetResource("Catalog.ViewMode.Grid"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=grid", null),
                    Selected = viewMode == "grid"
                });
                //list
                pagingFilteringModel.AvailableViewModes.Add(new SelectListItem
                {
                    Text = _localizationService.GetResource("Catalog.ViewMode.List"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=list", null),
                    Selected = viewMode == "list"
                });
            }

        }

        [NonAction]
        protected virtual void PreparePageSizeOptions(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command,
            bool allowCustomersToSelectPageSize, string pageSizeOptions, int fixedPageSize)
        {
            if (pagingFilteringModel == null)
                throw new ArgumentNullException("pagingFilteringModel");

            if (command == null)
                throw new ArgumentNullException("command");

            if (command.PageNumber <= 0)
            {
                command.PageNumber = 1;
            }
            pagingFilteringModel.AllowCustomersToSelectPageSize = false;
            if (allowCustomersToSelectPageSize && pageSizeOptions != null)
            {
                var pageSizes = pageSizeOptions.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (pageSizes.Any())
                {
                    // get the first page size entry to use as the default (category page load) or if customer enters invalid value via query string
                    if (command.PageSize <= 0 || !pageSizes.Contains(command.PageSize.ToString()))
                    {
                        int temp;
                        if (int.TryParse(pageSizes.FirstOrDefault(), out temp))
                        {
                            if (temp > 0)
                            {
                                command.PageSize = temp;
                            }
                        }
                    }

                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "pagesize={0}", null);
                    sortUrl = _webHelper.RemoveQueryString(sortUrl, "pagenumber");

                    foreach (var pageSize in pageSizes)
                    {
                        int temp;
                        if (!int.TryParse(pageSize, out temp))
                        {
                            continue;
                        }
                        if (temp <= 0)
                        {
                            continue;
                        }

                        pagingFilteringModel.PageSizeOptions.Add(new SelectListItem
                        {
                            Text = pageSize,
                            Value = String.Format(sortUrl, pageSize),
                            Selected = pageSize.Equals(command.PageSize.ToString(), StringComparison.InvariantCultureIgnoreCase)
                        });
                    }

                    if (pagingFilteringModel.PageSizeOptions.Any())
                    {
                        pagingFilteringModel.PageSizeOptions = pagingFilteringModel.PageSizeOptions.OrderBy(x => int.Parse(x.Text)).ToList();
                        pagingFilteringModel.AllowCustomersToSelectPageSize = true;

                        if (command.PageSize <= 0)
                        {
                            command.PageSize = int.Parse(pagingFilteringModel.PageSizeOptions.FirstOrDefault().Text);
                        }
                    }
                }
            }
            else
            {
                //customer is not allowed to select a page size
                command.PageSize = fixedPageSize;
            }

            //ensure pge size is specified
            if (command.PageSize <= 0)
            {
                command.PageSize = fixedPageSize;
            }
        }



        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false)
        {
            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);
        }


        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Vendor(int vendorId, CatalogPagingFilteringModel command)
        {
            int page = 1;
            if (page >= 1)
                page = page - 1;
            var vendor = _vendorService.GetVendorById(vendorId);
            if (vendor == null || vendor.Deleted || !vendor.Active)
                return InvokeHttp404();

            //Vendor is active?
            if (!vendor.Active)
                return InvokeHttp404();

            //'Continue shopping' URL
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.LastContinueShoppingPage,
                _webHelper.GetThisPageUrl(false),
                _storeContext.CurrentStore.Id);

            //Notification Items
            
            var customer = _bsVendorService.GetCustomerByVendorEmail(vendor.Email);
          //  var list = _bsVendorService.GetFollowVendorByCustomer(customer.Id, page, 10);
            if (customer == null) return null;

            var vendorNotifications = _bsVendorService.GetNotifications().Where(x=>x.NotifierCustomerId == customer.Id).ToList();
            var vendorLikeCount = _bsVendorService.GetLikeCountOfCustomer(customer.Id);
            var vendorFlowerCount = _bsVendorService.GetFollowCountOfVendor(vendorId);
            var totalProductCount = _bsVendorService.GetVendorProductCount(vendorId);
            var model = new Nop.Plugin.Bs.VendorSystem.Models.VendorModel
            {
                Id = vendor.Id,
                Name = vendor.GetLocalized(x => x.Name),
                Description = vendor.GetLocalized(x => x.Description),
                MetaKeywords = vendor.GetLocalized(x => x.MetaKeywords),
                MetaDescription = vendor.GetLocalized(x => x.MetaDescription),
                MetaTitle = vendor.GetLocalized(x => x.MetaTitle),
                SeName = vendor.GetSeName(),
                AllowCustomersToContactVendors = _vendorSettings.AllowCustomersToContactVendors,
                ProductCount = totalProductCount,
                ProductLikeCount = vendorLikeCount,
                ProductDislikeCount = vendorNotifications.Count(x => x.ItemTypeEnum == ItemType.ProductDislike),
                VendorFollowCount = vendorFlowerCount,
                VendorUnFollowCount = vendorNotifications.Count(x => x.ItemTypeEnum == ItemType.VendorUnFollow),
                ProductAddCount = vendorNotifications.Count(x => x.ItemTypeEnum == ItemType.ProductAdd),
                OrderPlacedCount = vendorNotifications.Count(x => x.ItemTypeEnum == ItemType.OrderPlaced)
            };



            //sorting
            PrepareSortingOptions(model.PagingFilteringContext, command);
            //view mode
            PrepareViewModes(model.PagingFilteringContext, command);
            //page size
            PreparePageSizeOptions(model.PagingFilteringContext, command,
                vendor.AllowCustomersToSelectPageSize,
                vendor.PageSizeOptions,
                vendor.PageSize);

            //products
            IList<int> filterableSpecificationAttributeOptionIds;
            var products = _productService.SearchProducts(out filterableSpecificationAttributeOptionIds, true,
                vendorId: vendor.Id,
                storeId: _storeContext.CurrentStore.Id,
                visibleIndividuallyOnly: true,
                orderBy: (ProductSortingEnum)command.OrderBy,
                pageIndex: command.PageNumber - 1,
                pageSize: command.PageSize);
            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);

            // return View(model);
            return View("~/Plugins/Bs.VendorSystem/Views/BsVendor/Vendor.cshtml", model);
        }
        #endregion


    }
}