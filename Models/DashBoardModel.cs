﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
   public class DashBoardModel
    {
       public string OrderCompletedTotal { get; set; }
       public string OrderPenddingTotal { get; set; }
       public string OrderPendingTotalQuintity { get; set; }
       public string VendorCreateTotalYearMonth { get; set; }
       public string VendorJoiningDate { get; set; }
    }
}
