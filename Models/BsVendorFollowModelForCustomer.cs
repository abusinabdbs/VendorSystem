﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
   public class BsVendorFollowModelForCustomer
    {
       
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CustomerName { get; set; }
        public string VendorSeName { get; set; }
        
        
    }
}
