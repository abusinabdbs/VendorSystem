﻿using Nop.Admin.Models.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace Nop.Plugin.Bs.VendorSystem.Models
{
    public class VendorProductModel
    {
        public VendorProductModel()
        {
            ProductModel = new ProductModel();
            AddProductAttributeCombinationModel = new AddProductAttributeCombinationModel();
            ProductAttributeValueListModel = new ProductModel.ProductAttributeValueListModel();
            ProductAttributeModel = new AddProductAttributeCombinationModel.ProductAttributeModel();
            ProductColor = new List<Option>();
            ProductSize = new List<Option>();
            AvailableRootCategories = new List<SelectListItem>();
            AvailableFirstSubCategories = new List<SelectListItem>();
            AvailableSecondSubCategories = new List<SelectListItem>();
        }
        public ProductModel ProductModel { get; set; }
        public ProductModel.ProductAttributeValueListModel ProductAttributeValueListModel { get; set; }
        //public ProductSpecificationAttributeModel ProductSpecificationAttributeModel { get; set; }
        //[NopResourceDisplayName("Admin.Catalog.Products.Fields.DealExpireDate")]
        //[UIHint("DateTimeNullable")]
        //public DateTime? DealExpireDate { get; set; }
        public AddProductAttributeCombinationModel AddProductAttributeCombinationModel { get; set; }
        public AddProductAttributeCombinationModel.ProductAttributeModel ProductAttributeModel { get; set; }
        public IList<SelectListItem> AvailableRootCategories { get; set; }
        public IList<SelectListItem> AvailableFirstSubCategories { get; set; }
        public IList<SelectListItem> AvailableSecondSubCategories { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.IsSecondHandProduct")]
        public bool IsSecondHandProduct { get; set; }

        public IList<Option> ProductColor { get; set; }

        public IList<Option> ProductSize { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
        [UIHint("Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
        //[UIHint("Picture")]
        public int PictureId1 { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
        [UIHint("Picture")]
        public int PictureId2 { get; set; }


  
        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
        [UIHint("Picture")]
        public int PictureId3 { get; set; }

     
        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
        [UIHint("Picture")]
        public int PictureId4 { get; set; }
        public int[] PictureIds { get; set; }
        public int[] SelectedCategoryIds { get; set; }
        public int SelectedCategoryId { get; set; }
        public int SelectedRootCategoryId { get; set; }
        public int SelectedFirstSubCategoryId { get; set; }
        public int SelectedSecondSubCategoryId { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Products.Fields.ProductLikeCount")]
        public int ProductLikeCount { get; set; }

        public partial class Option : BaseNopEntityModel
        {
            public string Name { get; set; }
           
            public int Quantity { get; set; }

            [NopResourceDisplayName("Admin.Catalog.Products.ProductAttributes.Attributes.Values.Fields.PriceAdjustment")]
            public decimal PriceAdjustment { get; set; }
        }


    }
   
}
