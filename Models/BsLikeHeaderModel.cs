﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    public class BsLikeHeaderModel
    {
        public bool IsGuest { get; set; }
        public int Count { get; set; }
    }
}
