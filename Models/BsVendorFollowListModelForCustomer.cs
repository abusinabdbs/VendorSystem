﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Common;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
  public class BsVendorFollowListModelForCustomer
    {
      public BsVendorFollowListModelForCustomer()
        {
            FollowList = new List<BsVendorFollowModelForCustomer>();
            //PagerModel = new PagerModel();
        }
            public IList<BsVendorFollowModelForCustomer> FollowList { get; set; }
            public PagerModel PagerModel { get; set; }
    }
}
