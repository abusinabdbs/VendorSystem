﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Plugin.Bs.VendorSystem.Validators;
using Nop.Web.Framework;
using FluentValidation.Attributes;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    [Validator(typeof(VendorRegisterValidator))]
    public class VendorRegisterModel
    {
        public VendorRegisterModel()
        {
            this.AvailableCountries = new List<SelectListItem>();
            this.AvailableStates = new List<SelectListItem>();
        }

        public int VendorId { get; set; }

        [NopResourceDisplayName("Account.Fields.FirstName")]
        [AllowHtml]
        public string FirstName { get; set; }
        [NopResourceDisplayName("Account.Fields.LastName")]
        [AllowHtml]
        public string LastName { get; set; }
        [NopResourceDisplayName("Account.Fields.Email")]
        [AllowHtml]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        [NopResourceDisplayName("Account.Fields.Password")]
        [AllowHtml]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [NopResourceDisplayName("Account.Fields.ConfirmPassword")]
        [AllowHtml]
        public string ConfirmPassword { get; set; }

        [NopResourceDisplayName("Account.Fields.StreetAddress")]
        [AllowHtml]
        public string StreetAddress { get; set; }

        [NopResourceDisplayName("Account.Fields.Country")]
        public int CountryId { get; set; }
        public IList<SelectListItem> AvailableCountries { get; set; }

        [NopResourceDisplayName("Account.Fields.StateProvince")]
        public int StateProvinceId { get; set; }
        public IList<SelectListItem> AvailableStates { get; set; }
        [NopResourceDisplayName("Account.Fields.Phone")]
        [AllowHtml]
        public string Phone { get; set; }
        [NopResourceDisplayName("Account.Fields.BankName")]
        public string BankName { get; set; }
        [NopResourceDisplayName("Account.Fields.BankAccountName")]
        public string BankAccountName { get; set; }
        [NopResourceDisplayName("Account.Fields.BankAccountNumber")]
        public string BankAccountNumber { get; set; }
        [NopResourceDisplayName("Account.Fields.AlternatePhoneNumber")]
        public string AlternatePhoneNumber { get; set; }
        [NopResourceDisplayName("Account.Fields.StoreName")]
        public string StoreName { get; set; }
        [NopResourceDisplayName("Account.Fields.Url")]
        public string Url { get; set; }
        [NopResourceDisplayName("Account.Fields.Describe")]
        public string Describe { get; set; }
    }
}
