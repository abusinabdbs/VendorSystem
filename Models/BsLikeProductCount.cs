﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Models
{
    public class BsLikeProductCount
    {
        public int Count { get; set; }
        public int ProductId { get; set; }
    }
}
