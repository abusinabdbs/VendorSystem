using System.Collections.Generic;
using System.Web.Routing;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Core.Domain.Tasks;
using Nop.Core.Data;
using System.Linq;
using Nop.Plugin.Bs.VendorSystem.Data;
using Nop.Services.Security;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Bs.VendorSystem
{
    /// <summary>
    /// Live person provider
    /// </summary>
    public class BsVendorPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {

        private readonly ISettingService _settingService;
        private readonly BsVendorObjectContext _objectContext;
        private const string ProductBoxWidget = "productbox_addinfo_before";
        private const string ProductDetailWidget = "productdetails_overview_top";
       // private const string HeaderLinkWidget = "header_links_after";
        
        private const string FollowHeaderLinkWidget = "header_links_before";
        private const string VendorFollowWidget = "vendordetails_top";
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;

        public BsVendorPlugin(ISettingService settingService, BsVendorObjectContext objectContext, IPermissionService permissionService, ILocalizationService localizationService)
        {
            this._settingService = settingService;
            this._objectContext = objectContext;
            this._permissionService = permissionService;
            _localizationService = localizationService;


        }

        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>Widget zones</returns>
        public IList<string> GetWidgetZones()
        {
            return new List<string>
            { 
               // "body_end_html_tag_before"
               ProductBoxWidget,
               ProductDetailWidget,
              // HeaderLinkWidget,
               VendorFollowWidget,
               FollowHeaderLinkWidget
            };
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "BsVendor";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Bs.VendorSystem.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for displaying widget
        /// </summary>
        /// <param name="widgetZone">Widget zone where it's displayed</param>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            //if (widgetZone.Equals(FollowHeaderLinkWidget))
            //{
            //    actionName = "FolowHeader";
            //    controllerName = "BsVendor";
            //    routeValues = new RouteValueDictionary
            //        {
            //            {"Namespaces", "Nop.Plugin.Bs.VendorSystem.Controllers"},
            //            {"area", null},
            //            {"widgetZone", widgetZone}
            //        };
            //}
            if (widgetZone.Equals(FollowHeaderLinkWidget))
            {
                actionName = "NotificationHeader";
                controllerName = "BsVendor";
                routeValues = new RouteValueDictionary
                    {
                        {"Namespaces", "Nop.Plugin.Bs.VendorSystem.Controllers"},
                        {"area", null},
                        {"widgetZone", widgetZone}
                    };
            }

            else if (widgetZone.Equals(VendorFollowWidget))
            {
                actionName = "FollowProductboxInfo";
                controllerName = "BsVendor";
                routeValues = new RouteValueDictionary
                    {
                        {"Namespaces", "Nop.Plugin.Bs.VendorSystem.Controllers"},
                        {"area", null},
                        {"widgetZone", widgetZone}
                    };
            }

            else
            {
                //if (widgetZone.Equals(HeaderLinkWidget))
                //{
                //    actionName = "LikeHeader";
                //    controllerName = "BsVendor";
                //    routeValues = new RouteValueDictionary
                //    {
                //        {"Namespaces", "Nop.Plugin.Bs.VendorSystem.Controllers"},
                //        {"area", null},
                //        {"widgetZone", widgetZone}
                //    };
                //}
                //else
                //{
                    actionName = "ProductboxInfo";
                    controllerName = "BsVendor";
                    routeValues = new RouteValueDictionary
                {
                    {"Namespaces", "Nop.Plugin.Bs.VendorSystem.Controllers"},
                    {"area", null},
                    {"widgetZone", widgetZone}
                };
               // }
            }



        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            _objectContext.Install();

            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.Liked", "Liked");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.Count", "Count");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.List", "Like List");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.DeleteSelected", "Delete Selected");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.ListEmpty", "Empty Like List");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.LikeTitle", "Like");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.UnLikeTitle", "Unlike");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.GuestTitle", "Log in to like");

            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.Liked", "Liked");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.Count", "Count");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.List", "Like List");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.DeleteSelected", "Delete Selected");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.ListEmpty", "Empty Like List");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.LikeTitle", "Like");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.UnLikeTitle", "Unlike");
            //this.AddOrUpdatePluginLocaleResource("Bs.VendorSystem.GuestTitle", "Log in to like");
            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            _objectContext.Uninstall();

            //this.DeletePluginLocaleResource("Bs.VendorSystem.Liked");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.Count");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.List");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.DeleteSelected");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.ListEmpty");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.LikeTitle");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.UnLikeTitle");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.GuestTitle");

            //this.DeletePluginLocaleResource("Bs.VendorSystem.Liked");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.Count");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.List");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.DeleteSelected");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.ListEmpty");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.LikeTitle");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.UnLikeTitle");
            //this.DeletePluginLocaleResource("Bs.VendorSystem.GuestTitle");

            base.Uninstall();
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItemBuilder = new SiteMapNode()
            {
                Visible = true,
                Title = _localizationService.GetResource("Vendor.Admin"),
                ActionName = "NonApprovedVendorList",
                ControllerName = "BsVendor",
                RouteValues = new RouteValueDictionary() { { "Area", "" } }
            };

            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party vendor plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItemBuilder);
            else
                rootNode.ChildNodes.Add(menuItemBuilder);
        }
    }
}
