﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Domain
{
   public enum ItemType
    {
        ProductLike = 100,
        ProductDislike = 200,
        VendorFollow = 300,
        VendorUnFollow = 400,
        ProductAdd = 500,
        OrderPlaced = 600

    }
}
