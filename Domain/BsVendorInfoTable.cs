﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Bs.VendorSystem.Domain
{
    public class BsVendorInfoTable : BaseEntity
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string StreetAddress { get; set; }
        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public string Phone { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string StoreName { get; set; }
        public string Url { get; set; }
        public string Describe { get; set; }
        public bool Approved { get; set; }
        public bool Deleted { get; set; }
        public int VendorId { get; set; }
    }
}
